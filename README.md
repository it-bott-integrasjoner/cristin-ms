# Cristin MS

Generates and pushes an XML file for Cristin based on SAP and IGA data


## Installation

[Poetry v1.2+](https://python-poetry.org/docs/) is used for dependency management.

    # Make sure poetry is installed, see docs
    poetry shell        # Start a shell, creating a virtual environment.
    poetry install      # Install dependencies from lock file

### pre-commit

This project uses [pre-commit](https://pre-commit.com/).  Run `pre-commit install`
after `poetry install` to install pre-commit hooks.


## Local configuration

Local configuration is read from `config.yaml` or from the file specified
in the environment variable `CRISTIN_MS_CONFIG`.

    $ cp config.example.yaml config.yaml
    # Edit the example config file

## Testing

    pytest

## Static Type Analysis

Use [mypy](http://mypy-lang.org/) to run static type checks using type hints.

    mypy .

## Docker

A `Dockerfile` is provided for application deployment purposes. It is not meant to be used for development.

`update-harbor-image.sh` is a utility script for building and uploading a Docker image to our private image repository `harbor.uio.no`.

It expects you to be logged in:

    docker login harbor.uio.no

Automated builds
---------------------------------------

Upon new commits on the master branch, gitlab will run tests, and if the tests pass, it will build new docker images, and tag them with "master-<gitsha>" and "latest".

Releasing
---------

You can also create a new tagged docker image by creating a release in gitlab.

When creating a new release in gitlab, the "create from" field should be set to `master` branch. The "tag name" of the release, should be of the format "v1.0.0", and cannot contain characters like dash (-) for example (because of the slugify function in the buildscript).

The tag name, will be used to tag the docker image, for easier reference.

You can then check in the [cristin-ms project on harbor](https://harbor.uio.no/harbor/projects/3915/repositories/cristin-ms), to see the new docker images.

(Note: gitlab is configured with two variables with harbor login info in order for this to work)

Configuration lives in the deploy-bott-int repo.

That is also where you would perform a deployment.

More deployment info in the [deploy-bott-int repository](https://bitbucket.usit.uio.no/projects/BOTTINT/repos/deploy-bott-int).
