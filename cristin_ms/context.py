from __future__ import annotations

import datetime
import logging.config
import typing
from functools import cached_property
from typing import assert_never

import httpx
import sentry_sdk
from abstract_iga_client.iga_abstract import IgaAbstract
from abstract_iga_client.iga_result import IgaResult
from dfo_sap_client import SapClient
from greg_client.client import Client as GregClient
from orgreg_client import Client as OrgregClient
from requests import Session
from requests.adapters import HTTPAdapter

from cristin_ms.greg import get_guest_employees
from cristin_ms.iga_data import SourceSystem
from cristin_ms.predicate.person_query_parser import parse_expression_query

if typing.TYPE_CHECKING:
    from collections.abc import Mapping

    import greg_client.models
    from dfo_sap_client.models import Ansatt, Orgenhet, Stilling
    from orgreg_client.models import ClientConfig, OrgUnit
    from urllib3 import Retry

    from cristin_ms.config import (
        CristinMsConfig,
        CristinMsConfigLoader,
        DfoSapClientConfig,
    )
    from cristin_ms.predicate import PredicateContext

logger = logging.getLogger(__name__)


def get_sap_client(config: DfoSapClientConfig) -> SapClient:
    return SapClient(**config.dict())


def get_orgreg_client(config: ClientConfig) -> OrgregClient:
    return OrgregClient(config)


class NotTried:
    pass


NOT_TRIED = NotTried()


class CristinMsContext:
    """Container for singleton objects.

    The general idea is to pass around this object, so that its attributes acts
    as singletons.
    """

    def __init__(
        self,
        config_loader: CristinMsConfigLoader,
        *,
        now: datetime.datetime | None = None,
    ) -> None:
        self.config_loader = config_loader
        self.iga_cache: dict[SourceSystem, dict[str, IgaResult | None]] = {}
        self.__post_init__()
        self.__supports_fetch_all_from_iga = False
        self.now = now or datetime.datetime.today()

    def __post_init__(self) -> None:
        try:
            if logging_cfg := self.config_loader.logging:
                logging.config.dictConfig(logging_cfg.dict(exclude_unset=True))
            else:
                logging.basicConfig()
        except Exception:  # pragma: no cover
            logger.exception("Logging configuration failed")

        try:
            if sentry_cfg := self.config_loader.sentry:
                logger.info("Sentry enabled, initializing")
                sentry_sdk.init(**sentry_cfg.dict(exclude_unset=True))
                logger.info("Sentry initialized")
            else:
                logger.info("Sentry configuration not found")
        except Exception:  # pragma: no cover
            logger.exception("Sentry configuration failed")

    @cached_property
    def config(self) -> CristinMsConfig:
        return self.config_loader.cristin_ms

    @cached_property
    def iga(self) -> IgaAbstract:
        """Abstract IGA client from configuration."""
        return IgaAbstract.get_iga_client(
            config=self.config.iga,
            session=self.requests_session,
        )

    @cached_property
    def sap(self) -> SapClient:
        """dfo_sap_client.client.SapClient object from configuration."""
        return get_sap_client(self.config.sap)

    @cached_property
    def orgreg(self) -> OrgregClient:
        """dfo_sap_client.client.SapClient object from configuration."""
        return get_orgreg_client(self.config.orgreg)

    @cached_property
    def greg(self) -> GregClient:
        """GregClient object from configuration."""
        if not self.config.guests:
            raise ValueError
        return GregClient(
            base_url=self.config.guests.greg.base_url,
            headers=self.config.guests.greg.headers or {},
            timeout=httpx.Timeout(10.0),
            raise_on_unexpected_status=True,
        )

    @cached_property
    def orgreg_ous_by_id(self) -> Mapping[int, OrgUnit]:
        logger.info("Fetching org units from OrgReg")
        xs = self.orgreg.get_ou()
        if not xs:
            raise ValueError
        return {x.ou_id: x for x in xs}

    @property
    def urllib3_retry(self) -> Retry:
        return self.config.http_retry.get_urllib3_retry()

    @property
    def requests_session(self) -> Session:
        session = Session()
        session.mount("https://", HTTPAdapter(max_retries=self.urllib3_retry))
        session.mount("http://", HTTPAdapter(max_retries=self.urllib3_retry))
        return session

    def get_iga_data(self, source_system: SourceSystem, id_: str) -> IgaResult | None:
        if not isinstance(id_, str):
            raise TypeError
        if source_system == SourceSystem.GREG and not self.config.guests:
            msg = "Trying to get GREG person when guests are not configured"
            raise ValueError(msg)
        iga_result: IgaResult | None

        if source_system not in self.iga_cache:
            self.init_iga_cache(source_system)
        cached_result = self.iga_cache[source_system].get(
            id_,
            None if self.__supports_fetch_all_from_iga else NOT_TRIED,
        )
        match cached_result:
            case NotTried():
                iga_result = self.get_iga_result(source_system, id_)
                self.iga_cache[source_system][id_] = iga_result
            case IgaResult() as x:
                iga_result = x
            case None:
                iga_result = None
            case _:
                assert_never(cached_result)

        if not iga_result:
            logger.info("Couldn't find IGA data for id %s in source system %s", id_, source_system)

        return iga_result

    def init_iga_cache(self, source_system: SourceSystem) -> None:
        if source_system in self.iga_cache:
            msg = f"Cache for source system {source_system} is already initialized"
            raise ValueError(msg)
        try:
            logger.info("Attempting to create iga cache for source system %s", source_system)
            match source_system:
                case SourceSystem.SAP:
                    logger.info("Trying to fetch all employees from IGA")
                    self.iga_cache[source_system] = {
                        i.sap_person_id.zfill(8): i
                        for i in self.iga.get_all_employees()  # type: ignore[union-attr]
                        if i and i.sap_person_id
                    }
                case SourceSystem.GREG:
                    logger.info("Trying to fetch all greg persons from IGA")
                    self.iga_cache[source_system] = {
                        i.greg_person_id: i
                        for i in self.iga.get_all_greg_persons()
                        if i and i.greg_person_id
                    }
                case _:
                    typing.assert_never(source_system)
        except NotImplementedError:
            logger.info("Fetch all not implemented for source system %s", source_system)
            self.iga_cache[source_system] = {}
            self.__supports_fetch_all_from_iga = False
        else:
            self.__supports_fetch_all_from_iga = True

    def get_iga_result(self, source_system: SourceSystem, value: str | int) -> IgaResult | None:
        match source_system:
            case SourceSystem.SAP:
                return self.iga.get_person_by_employee_nr(value)
            case SourceSystem.GREG:
                return self.iga.get_person_by_greg_person_id(int(value))
            case _:
                typing.assert_never(source_system)

    @cached_property
    def sap_ansatte_by_id(self) -> Mapping[str, Ansatt]:
        logger.info("Fetching SAP ansatte")
        xs = self.sap.get_all_ansatte()
        if not xs:
            raise ValueError
        return {x.id: x for x in xs}

    @cached_property
    def sap_stillinger_by_id(self) -> Mapping[int, Stilling]:
        logger.info("Fetching SAP stillinger")
        xs = self.sap.get_all_stillinger()
        if not xs:
            raise ValueError
        return {x.id: x for x in xs}

    @cached_property
    def sap_orgenheter_by_id(self) -> Mapping[int, Orgenhet]:
        logger.info("Fetching SAP orgenheter")
        xs = self.sap.get_all_orgenheter()
        if not xs:
            raise ValueError
        return {x.id: x for x in xs}

    @cached_property
    def greg_persons_by_id(self) -> Mapping[int, greg_client.models.Person]:
        if self.config.guests:
            logger.info("Fetching GREG persons")
            with self.greg as client:
                data = {x.id: x for x in get_guest_employees(client=client)}
            if not data:
                raise ValueError
            return data
        return {}

    @cached_property
    def greg_person_filter(self) -> typing.Callable[[PredicateContext], bool]:
        if self.config.guests is None:
            msg = "Guests are not configured"
            raise TypeError(msg)
        return parse_expression_query(self.config.guests.greg_person_filter)

    @cached_property
    def sap_ansatt_filter(self) -> typing.Callable[[PredicateContext], bool]:
        return parse_expression_query(self.config.ansatt_filter)

    @property
    def frida_import_dato(self) -> datetime.date:
        """Date used in fridaImport/beskrivelse/dato.

        This is a hack to use the next when the job was started before midnight.
        """
        current_datetime = self.now
        current_time = current_datetime.time()
        export_date = current_datetime.date()
        if current_time > self.config.export_time:
            export_date += datetime.timedelta(days=1)
        return export_date
