from __future__ import annotations

import abc
import dataclasses
from collections.abc import Callable
from typing import TYPE_CHECKING, Generic, TypeAlias, TypeVar, override

if TYPE_CHECKING:
    from collections.abc import Iterable

__all__ = [
    "BoolAlways",
    "BoolAnd",
    "BoolExpr",
    "BoolNot",
    "BoolOr",
    "Predicate",
]

T = TypeVar("T")
Predicate: TypeAlias = Callable[[T], bool]


class BoolExpr(Generic[T], abc.ABC):
    @abc.abstractmethod
    def __call__(self, arg: T) -> bool:
        """Evaluate expression."""


@dataclasses.dataclass(frozen=True, repr=True, eq=True)
class BoolAlways(BoolExpr[T]):
    value: bool

    @override
    def __call__(self, arg: T) -> bool:
        return self.value


@dataclasses.dataclass(frozen=True, repr=True, eq=True)
class BoolNot(BoolExpr[T]):
    predicate: Predicate[T]

    @override
    def __call__(self, arg: T) -> bool:
        v = bool(self.predicate(arg))
        return not v


@dataclasses.dataclass(frozen=True, repr=True, eq=True)
class BoolAnd(BoolExpr[T]):
    predicates: Iterable[Predicate[T]]

    @override
    def __call__(self, arg: T) -> bool:
        return all(p(arg) for p in self.predicates)


@dataclasses.dataclass(frozen=True, repr=True, eq=True)
class BoolOr(BoolExpr[T]):
    predicates: Iterable[Predicate[T]]

    @override
    def __call__(self, arg: T) -> bool:
        return any(p(arg) for p in self.predicates)
