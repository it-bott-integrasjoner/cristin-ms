from __future__ import annotations

import dataclasses
import datetime
import enum
import logging
from collections.abc import Iterable
from typing import Any, Generic, TypeAlias, TypeVar, assert_never, override

import pyparsing as pp
from pyparsing.common import pyparsing_common as ppc

from cristin_ms.predicate import (
    NullValue,
    PredicateABC,
    PredicateContext,
)

logger = logging.getLogger(__name__)
T = TypeVar("T")

pp.ParserElement.enable_packrat()

ACCESS_OPERATOR = "::"


@dataclasses.dataclass(kw_only=True, frozen=True, repr=True, eq=True)
class Path:
    keys: tuple[int | str, ...]

    @override
    def __str__(self) -> str:
        return ".".join(map(str, self.keys))


@dataclasses.dataclass(kw_only=True, frozen=True, repr=True, eq=True)
class GetValue(Generic[T]):
    """GetValue.

    >>> GetValue(tag="O", path=Path(keys=("name",)))({"name": "Tor"})
    'Tor'
    >>> GetValue(tag="O", path=Path(keys=(0, "name")))([{"name": "Espen"}])
    'Espen'
    >>> GetValue(tag="O", path=Path(keys=(0, "name")))([{}])
    >>> GetValue(tag="O", path=Path(keys=(0, "name")))([])
    >>> GetValue(tag="O", path=Path(keys=(0, "name")))({})
    >>> GetValue(tag="O", path=Path(keys=(0, "name")))(object())
    >>> str(GetValue(tag="O", path=Path(keys=("name",))))
    'O::name'
    """

    tag: T
    path: Path

    def __call__(self, data: Any) -> Any:
        for k in self.path.keys:
            if hasattr(data, "__getitem__"):
                try:
                    data = data[k]
                except (KeyError, IndexError):
                    return None
            else:
                try:
                    data = getattr(data, str(k))
                except AttributeError as exc:
                    logger.info("%s", exc)
                    return None
        return data

    @override
    def __str__(self) -> str:
        return f"{self.tag}{ACCESS_OPERATOR}{self.path}"


DOUBLE_COLON, DOT = pp.Suppress(ACCESS_OPERATOR), pp.Suppress(".")
property_name = pp.Word(pp.alphanums + "_")
quoted_string = pp.QuotedString('"') ^ pp.QuotedString("'")
key_element = ppc.integer ^ quoted_string ^ property_name
path = pp.Group(
    pp.delimited_list(
        key_element,
        min=1,
        delim=DOT,
    ),
    aslist=True,
).set_parse_action(lambda toks: Path(keys=toks[0]))


def get_value_parser(tag: pp.ParserElement | str) -> pp.ParserElement:
    """get_value_parser.

    >>> get_value_parser(pp.Keyword("Token")).parse_string('Token::0.emp."name"', parse_all=True)
    ParseResults([GetValue(tag='Token', path=Path(keys=[0, 'emp', 'name']))], {})

    >>> get_value_parser("StrToken").parse_string('StrToken::0.emp."name"', parse_all=True)
    ParseResults([GetValue(tag='StrToken', path=Path(keys=[0, 'emp', 'name']))], {})

    >>> get_value_parser("OtherToken").parse_string('Token::0.emp."name"', parse_all=True)
    Traceback (most recent call last):
    ...
    pyparsing.exceptions.ParseException: Expected Keyword 'OtherToken', found 'Token'  (at char 0), (line:1, col:1)
    """
    if isinstance(tag, str):
        tag = pp.Keyword(tag)
    return (tag + DOUBLE_COLON + path).set_parse_action(
        lambda toks: GetValue(tag=toks[0], path=toks[1]),
    )


Value: TypeAlias = (
    GetValue[Any] | int | str | float | datetime.date | Iterable[int] | Iterable[str] | None
)


class BinaryOperator(enum.StrEnum):
    EQ = "="
    NE = "!="
    GE = ">="
    GT = ">"
    LE = "<="
    LT = "<"
    IN = "in"

    def __call__(self, a: Any, b: Any) -> Any:
        match self:
            case BinaryOperator.EQ:
                return a == b
            case BinaryOperator.NE:
                return a != b
            case BinaryOperator.GE:
                return a >= b
            case BinaryOperator.GT:
                return a > b
            case BinaryOperator.LE:
                return a <= b
            case BinaryOperator.LT:
                return a < b
            case BinaryOperator.IN:
                return a in b
            case _:
                assert_never(self)


def bin_op(op: BinaryOperator) -> pp.ParserElement:
    return pp.Keyword(str(op)).set_parse_action(lambda _: op)


# fmt: off
binary_operator = (
    pp.Keyword("==").set_parse_action(lambda _: BinaryOperator.EQ)
    ^ pp.Or([bin_op(op) for op in BinaryOperator])
)
# fmt: on


@dataclasses.dataclass(frozen=True, repr=True, eq=True, kw_only=True)
class Expression(PredicateABC):
    """Expression.

    >>> import operator
    >>> import datetime
    >>> from dfo_sap_client.models import Innehaver
    >>> from cristin_ms.predicate import ObjectTag, negative_results_formatted
    >>> expr = Expression(a="1", operator=BinaryOperator.EQ, b=GetValue(tag=ObjectTag.INNEHAVER, path=Path(keys=("innehaver_ansattnr",))))
    >>> context = PredicateContext(today=datetime.date.today(), get_innehaver=lambda: innehaver)
    >>> innehaver = Innehaver(innehaver_ansattnr="1", innehaver_startdato="2020-01-01", innehaver_sluttdato="2020-12-31")
    >>> expr(context)
    True
    >>> innehaver = Innehaver(innehaver_ansattnr="12", innehaver_startdato="2020-01-01", innehaver_sluttdato="2020-12-31")
    >>> expr(context)
    False
    >>> a = GetValue(tag=ObjectTag.INNEHAVER, path=Path(keys=("innehaver_ansattnr",)))
    >>> b = ["1", "12"]
    >>> expr = Expression(a=a, b=b, operator=BinaryOperator.IN)
    >>> expr(context)
    True
    >>> Expression(a=1, b=2, operator=12)
    Traceback (most recent call last):
    ...
    TypeError: Invalid operator
    >>> expr = Expression(a=1, b=2, operator=lambda a, b: 12)
    Traceback (most recent call last):
    ...
    TypeError: Invalid operator
    >>> a = GetValue(tag=ObjectTag.INNEHAVER, path=Path(keys=("innehaver_sluttdato",)))
    >>> b = innehaver.innehaver_sluttdato
    >>> expr = Expression(a=a, b=b, operator=BinaryOperator.EQ)
    >>> expr(context)
    True
    >>> context.results
    [(Expression(a='1', b=GetValue(tag=<ObjectTag.INNEHAVER: 'Innehaver'>, path=Path(keys=('innehaver_ansattnr',))), operator=<BinaryOperator.EQ: '='>), True), (Expression(a='1', b=GetValue(tag=<ObjectTag.INNEHAVER: 'Innehaver'>, path=Path(keys=('innehaver_ansattnr',))), operator=<BinaryOperator.EQ: '='>), False), (Expression(a=GetValue(tag=<ObjectTag.INNEHAVER: 'Innehaver'>, path=Path(keys=('innehaver_ansattnr',))), b=['1', '12'], operator=<BinaryOperator.IN: 'in'>), True), (Expression(a=GetValue(tag=<ObjectTag.INNEHAVER: 'Innehaver'>, path=Path(keys=('innehaver_sluttdato',))), b=datetime.date(2020, 12, 31), operator=<BinaryOperator.EQ: '='>), True)]
    >>> negative_results_formatted(context.results)
    ["'1' = Innehaver::innehaver_ansattnr"]
    """

    a: Value
    b: Value
    operator: BinaryOperator

    def __post_init__(self) -> None:
        if not isinstance(self.operator, BinaryOperator):
            msg = "Invalid operator"
            raise TypeError(msg)

    @staticmethod
    def _extract_value(*, context: PredicateContext, value: Value) -> Any:
        if isinstance(value, GetValue):
            v = value(context.get_value(value.tag))
            if isinstance(v, datetime.date):
                return str(v)
            if isinstance(v, list):
                v = tuple(v)
            return v

        if isinstance(value, list):
            return tuple(value)
        if isinstance(value, datetime.date):
            return str(value)
        if isinstance(value, NullValue):
            return None
        return value

    @override
    def do_call(self, ctx: PredicateContext) -> bool:
        a = self._extract_value(context=ctx, value=self.a)
        b = self._extract_value(context=ctx, value=self.b)
        op = self.operator
        res = op(a, b)
        if not isinstance(res, bool):
            msg = "Result is not a bool"
            raise TypeError(msg)
        return res

    @override
    def __str__(self) -> str:
        a = str(self.a) if isinstance(self.a, NullValue | GetValue) else repr(self.a)
        b = str(self.b) if isinstance(self.b, NullValue | GetValue) else repr(self.b)
        return f"{a} {self.operator} {b}"
