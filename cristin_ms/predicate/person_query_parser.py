"""Parser.

>>> ansatt.parse_string("Ansatt")
ParseResults([<class 'dfo_sap_client.models.Ansatt'>], {})

>>> query = "Ansatt::etternavn != 'Pettersen'"
>>> expr = parse_expression_query("Ansatt::etternavn != 'Pettersen'")
>>> expr
Expression(a=GetValue(tag=<ObjectTag.ANSATT: 'Ansatt'>, path=Path(keys=['etternavn'])), b='Pettersen', operator=<BinaryOperator.NE: '!='>)
>>> str(expr) == query
True
"""
from __future__ import annotations

from typing import TYPE_CHECKING

import abstract_iga_client.iga_result as iga_models
import dfo_sap_client.models as sap_models
import pyparsing as pp
from pyparsing.common import pyparsing_common as ppc

import cristin_ms.predicate.functions as function_predicates
from cristin_ms.predicate import (
    NullValue,
    ObjectTag,
    PredicateABC,
    PredicateContext,
)
from cristin_ms.predicate.eval import (
    BoolAlways,
    BoolAnd,
    BoolNot,
    BoolOr,
)
from cristin_ms.predicate.get_value import Expression, binary_operator, get_value_parser

if TYPE_CHECKING:
    from collections.abc import Callable

pp.ParserElement.enable_packrat()

__all__ = [
    "parse_expression_query",
]


NULL_VALUE = NullValue()


def predicate_parser(pred: type[PredicateABC]) -> pp.ParserElement:
    return pp.Keyword(pred.__name__).set_parse_action(lambda _: pred)


LBRACK, RBRACK, DOT, COMMA = map(pp.Suppress, "[].,")

property_name = pp.Keyword(pp.alphanums + "_")

# fmt: off
quoted_string = pp.Or(pp.QuotedString(c, esc_char="\\") for c in ['"', "'"])
item = quoted_string ^ ppc.integer
element_list = LBRACK + pp.delimited_list(item, min=1) + RBRACK

ansatt_tag = pp.Keyword(ObjectTag.ANSATT)
stilling_tag = pp.Keyword(ObjectTag.STILLING)
innehaver_tag = pp.Keyword(ObjectTag.INNEHAVER)
iga_result_tag = pp.Keyword(ObjectTag.IGA_RESULT)
greg_person_tag = pp.Keyword(ObjectTag.GREG_PERSON)
greg_role_tag = pp.Keyword(ObjectTag.GREG_ROLE)

ansatt_value = get_value_parser(ansatt_tag)
stilling_value = get_value_parser(stilling_tag)
innehaver_value = get_value_parser(innehaver_tag)
iga_result_value = get_value_parser(iga_result_tag)
greg_person_value = get_value_parser(greg_person_tag)

ansatt = pp.Keyword("Ansatt").set_parse_action(lambda _: sap_models.Ansatt)
stilling = pp.Keyword("Stilling").set_parse_action(lambda _: sap_models.Stilling)
iga_result = pp.Keyword("IgaResult").set_parse_action(lambda _: iga_models.IgaResult)

null = pp.Keyword("null").set_parse_action(lambda _: NULL_VALUE)

boolean = (
    pp.Keyword("true").set_parse_action(lambda _: BoolAlways(value=True))
    ^ pp.Keyword("false").set_parse_action(lambda _: BoolAlways(value=False))
)

value = (
    ansatt_value
    ^ stilling_value
    ^ iga_result_value
    ^ innehaver_value
    ^ greg_person_value
    ^ pp.Group(element_list, aslist=True)
    ^ item
    ^ null
    ^ boolean
)

expression = (value + binary_operator + value).set_parse_action(
    lambda toks: Expression(a=toks[0], operator=toks[1], b=toks[2]),
)

has_cerebrum_consent = (
    predicate_parser(function_predicates.HasCerebrumConsent)
    + LBRACK
    + pp.Suppress("name=") + quoted_string
    + COMMA
    + pp.Suppress("type=") + quoted_string
    + RBRACK
).set_parse_action(lambda toks: toks[0](name=toks[1], type_=toks[2]))

base_predicate = (
    (
        (ansatt_tag ^ greg_person_tag)
        + pp.Suppress(function_predicates.ACCESS_OPERATOR)
        + predicate_parser(function_predicates.IsFnrValid)
    ).add_parse_action(lambda toks: toks[1](tag=toks[0]))
    ^ (
        (ansatt_tag ^ greg_role_tag)
        + pp.Suppress(function_predicates.ACCESS_OPERATOR)
        + predicate_parser(function_predicates.IsEmployed)
    ).add_parse_action(lambda toks: toks[1](tag=toks[0]))
    ^ expression
    ^ has_cerebrum_consent
)

# fmt: on


def create_expression(base_exp: pp.ParserElement) -> pp.ParserElement:
    return (
        pp.infix_notation(
            base_exp,
            [
                ("!", 1, pp.OpAssoc.RIGHT, lambda t: BoolNot(t[0][1])),
                ("&&", 2, pp.OpAssoc.LEFT, lambda t: BoolAnd(t[0][0::2])),
                ("||", 2, pp.OpAssoc.LEFT, lambda t: BoolOr(t[0][0::2])),
            ],
        )
        ^ boolean
    ).ignore("#" + pp.rest_of_line)


def parse_expression_query(s: str) -> Callable[[PredicateContext], bool]:
    """parse_expression_query.

    Examples
    --------
    >>> query = "GregPerson->IsFnrValid"
    >>> expr = parse_expression_query(query)
    >>> expr
    IsFnrValid(tag=<ObjectTag.GREG_PERSON: 'GregPerson'>)
    >>> str(expr) == query
    True

    >>> query = "GregRole->IsEmployed"
    >>> expr = parse_expression_query(query)
    >>> expr
    IsEmployed(tag=<ObjectTag.GREG_ROLE: 'GregRole'>)
    >>> str(expr) == query
    True

    >>> query = "Ansatt->IsFnrValid"
    >>> expr = parse_expression_query(query)
    >>> expr
    IsFnrValid(tag=<ObjectTag.ANSATT: 'Ansatt'>)
    >>> str(expr) == query
    True

    >>> query = "HasCerebrumConsent[name='cerebrum', type='opt-in']"
    >>> expr = parse_expression_query(query)
    >>> expr
    HasCerebrumConsent(tag=<ObjectTag.IGA_RESULT: 'IgaResult'>, name='cerebrum', type_='opt-in')
    >>> str(expr) == query
    True
    """
    try:
        results = create_expression(base_predicate).parse_string(s, parse_all=True)
    except pp.ParseException as exc:
        raise ValueError(str(exc)) from exc
    if len(results) != 1:
        msg = "Expected exactly 1 result"
        raise AssertionError(msg)
    return results[0]  # type: ignore[no-any-return]
