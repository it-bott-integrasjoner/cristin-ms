from __future__ import annotations

import abc
import dataclasses
import logging
from abc import abstractmethod
from enum import StrEnum
from typing import (
    TYPE_CHECKING,
    assert_never,
    final,
    override,
)

if TYPE_CHECKING:
    import datetime
    from collections.abc import Callable, Iterable

    import abstract_iga_client.iga_result as iga_models
    import dfo_sap_client.models as sap_models
    import greg_client.models as greg_models

logger = logging.getLogger(__name__)


class PredicateABC(abc.ABC):
    """A simple predicate."""

    @final
    def __call__(self, ctx: PredicateContext) -> bool:
        result = self.do_call(ctx)
        logger.debug("%r() = %r", self, result)
        ctx.results.append((self, result))
        return result

    @abstractmethod
    def do_call(self, ctx: PredicateContext) -> bool:
        """Execute predicate."""


@final
class NullValue:
    @override
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}()"

    @override
    def __str__(self) -> str:
        return "null"


@final
class ObjectTag(StrEnum):
    ANSATT = "Ansatt"
    STILLING = "Stilling"
    INNEHAVER = "Innehaver"
    IGA_RESULT = "IgaResult"
    GREG_PERSON = "GregPerson"
    GREG_ROLE = "GregRole"


@final
@dataclasses.dataclass(frozen=True, repr=True, eq=True, kw_only=True)
class PredicateContext:
    today: datetime.date
    get_ansatt: Callable[[], sap_models.Ansatt | None] = lambda: None
    get_stilling: Callable[[], sap_models.Stilling | None] = lambda: None
    get_innehaver: Callable[[], sap_models.Innehaver | None] = lambda: None
    get_iga_result: Callable[[], iga_models.IgaResult | None] = lambda: None
    get_consents: Callable[[], Iterable[object]] = lambda: ()
    get_greg_person: Callable[[], greg_models.Person | None] = lambda: None
    get_greg_role: Callable[[], greg_models.Role | None] = lambda: None
    results: list[tuple[PredicateABC, bool]] = dataclasses.field(default_factory=list)

    def get_value(
        self,
        tag: ObjectTag,
    ) -> (
        sap_models.Ansatt
        | sap_models.Stilling
        | sap_models.Innehaver
        | iga_models.IgaResult
        | Iterable[object]
        | greg_models.Person
        | greg_models.Role
        | None
    ):
        match tag:
            case ObjectTag.ANSATT:
                return self.get_ansatt()
            case ObjectTag.STILLING:
                return self.get_stilling()
            case ObjectTag.INNEHAVER:
                return self.get_innehaver()
            case ObjectTag.IGA_RESULT:
                return self.get_iga_result()
            case ObjectTag.GREG_PERSON:
                return self.get_greg_person()
            case ObjectTag.GREG_ROLE:
                return self.get_greg_role()
            case _:  # pragma: no cover
                assert_never(tag)


def negative_results_formatted(results: Iterable[tuple[PredicateABC, bool]]) -> list[str]:
    return [str(pred) for pred, res in results if not res]
