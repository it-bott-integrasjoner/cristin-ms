from __future__ import annotations

import abc
import dataclasses
import datetime
import logging
from typing import TypeVar, assert_never, override

import greg_client.models as greg_models
import greg_client.types
import stdnum.exceptions
import stdnum.no.fodselsnummer

from cristin_ms.greg_person import extract_greg_person_data
from cristin_ms.predicate import ObjectTag, PredicateABC, PredicateContext
from cristin_ms.predicate.exceptions import DateOutOfRangeError

logger = logging.getLogger(__name__)
T = TypeVar("T")


DAYS_BEFORE = 14
DAYS_AFTER = 3

ACCESS_OPERATOR = "->"


def check_dates_in_range(
    *,
    date: datetime.date,
    startdato: datetime.date | None | greg_client.types.Unset,
    sluttdato: datetime.date,
    days_before: int = DAYS_BEFORE,
    days_after: int = DAYS_AFTER,
) -> None:
    """Raise error if start or end date not okay."""
    if not startdato or isinstance(startdato, greg_client.types.Unset):
        msg = "startdato is nullish"
        raise DateOutOfRangeError(msg)
    if (date + datetime.timedelta(days=days_before)) < startdato:
        msg = f"It's more than {days_before} days until startdato"
        raise DateOutOfRangeError(msg)
    if (date - datetime.timedelta(days=days_after)) > sluttdato:
        msg = f"It's more than {days_after} days since sluttdato"
        raise DateOutOfRangeError(msg)


@dataclasses.dataclass(kw_only=True, frozen=True, repr=True, eq=True)
class FunctionPredicate(PredicateABC, abc.ABC):
    tag: ObjectTag

    @override
    def __str__(self) -> str:
        return f"{self.tag}{ACCESS_OPERATOR}{self.__class__.__name__}"


@dataclasses.dataclass(kw_only=True, frozen=True, repr=True, eq=True)
class IsFnrValid(FunctionPredicate):
    @override
    def do_call(self, ctx: PredicateContext) -> bool:
        match self.tag:
            case ObjectTag.ANSATT:
                ansatt = ctx.get_ansatt()
                if ansatt is None:
                    raise TypeError
                try:
                    stdnum.no.fodselsnummer.validate(ansatt.fnr)
                except stdnum.exceptions.ValidationError as exc:
                    logger.info("Invalid fnr for ansatt %s: %s", ansatt.id, exc)
                    return False
                else:
                    return True
            case ObjectTag.GREG_PERSON:
                person = ctx.get_greg_person()
                if person is None:
                    raise TypeError
                greg_person_data = extract_greg_person_data(person)
                try:
                    stdnum.no.fodselsnummer.validate(greg_person_data.fnr)
                except stdnum.exceptions.ValidationError as exc:
                    logger.info("Invalid fnr for GREG person %s: %s", person.id, exc)
                    return False
                else:
                    return True
            case ObjectTag() as x:
                msg = f"Tag {x} not supported"
                raise ValueError(msg)
            case _:
                assert_never(self.tag)


@dataclasses.dataclass(kw_only=True, frozen=True, repr=True, eq=True)
class IsEmployed(FunctionPredicate):
    @override
    def do_call(self, ctx: PredicateContext) -> bool:
        match self.tag:
            case ObjectTag.ANSATT:
                ansatt = ctx.get_ansatt()
                if ansatt is None:
                    raise TypeError
                try:
                    check_dates_in_range(
                        date=ctx.today,
                        startdato=ansatt.startdato,
                        sluttdato=ansatt.sluttdato,
                    )
                except DateOutOfRangeError as exc:
                    logger.info("Date check failed ansatt %s: %s", ansatt.id, exc)
                    return False
                else:
                    return True
            case ObjectTag.GREG_ROLE:
                role: greg_models.Role | None = ctx.get_greg_role()
                if role is None:
                    raise TypeError
                try:
                    check_dates_in_range(
                        date=ctx.today,
                        startdato=role.start_date,
                        sluttdato=role.end_date,
                    )
                except DateOutOfRangeError as exc:
                    if logger.isEnabledFor(logging.INFO):
                        if person := ctx.get_greg_person():
                            person_id = person.id
                        else:
                            person_id = None
                        logger.info(
                            "Date check failed GREG person %s, role %s: %s",
                            person_id,
                            role.id,
                            exc,
                        )
                    return False
                else:
                    return True
            case x:
                msg = f"Tag {x} not supported"
                raise ValueError(msg)


@dataclasses.dataclass(kw_only=True, frozen=True, repr=True, eq=True)
class HasCerebrumConsent(FunctionPredicate):
    tag: ObjectTag = ObjectTag.IGA_RESULT
    name: str
    type_: str

    @override
    def do_call(self, ctx: PredicateContext) -> bool:
        consents = ctx.get_consents()
        return self.name in {i.name for i in consents if i.type == self.type_}  # type: ignore[attr-defined]

    @override
    def __str__(self) -> str:
        return f"{self.__class__.__name__}[name={self.name!r}, type={self.type_!r}]"
