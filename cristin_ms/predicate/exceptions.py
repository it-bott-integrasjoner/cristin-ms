from __future__ import annotations


class DateOutOfRangeError(ValueError):
    pass
