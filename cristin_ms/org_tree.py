from __future__ import annotations

import dataclasses
import logging
from typing import TYPE_CHECKING, Any, final

from bottint_tree import Tree, build_forest

from cristin_ms.orgreg import (
    build_orgreg_by_stedkode,
    extract_dfo_org_ids,
    extract_stedkode,
)
from cristin_ms.types import ExportOrg

if TYPE_CHECKING:
    from collections.abc import Callable, Iterable, Iterator, Mapping

    from dfo_sap_client.models import Orgenhet
    from orgreg_client import OrgUnit

    from cristin_ms.config import CristinMsConfig
    from cristin_ms.stedkode import Stedkode

logger = logging.getLogger(__name__)


@final
class SapOrgTree:
    """An organization tree.

    You can look up organizations by their SAP organization id.
    """

    def __init__(self, orgs: Iterable[Orgenhet]) -> None:
        def id_key(x: Orgenhet) -> int:
            return x.id

        def parent_key(x: Orgenhet) -> int | None:
            return x.overordn_orgenhet_id

        forest = build_forest(
            orgs,
            id_key=id_key,
            parent_key=parent_key,
        )
        roots: list[int] = [x.id for x in forest.values() if x.parent is None]
        if len(roots) != 1:
            msg = "Expected org tree to have exactly 1 root node"
            raise ValueError(msg)
        self.__root: Tree[Orgenhet] = forest[roots[0]]
        self.__cache: Mapping[int, Tree[Orgenhet]] = forest

    @property
    def root(self) -> Tree[Orgenhet]:
        return self.__root

    def __getitem__(self, item: int) -> Tree[Orgenhet]:
        """Get tree by SAP organization ID."""
        return self.__cache[item]

    def __contains__(self, item: int) -> bool:
        """Test if forest contains tree with given SAP organization ID."""
        return item in self.__cache

    def __iter__(self) -> Iterator[Tree[Orgenhet]]:
        return iter(self.__cache.values())


class ExcludedByConfigError(ValueError):
    pass


class LevelTooLarge(ValueError):
    pass


class NoStedkodeError(ValueError):
    pass


@dataclasses.dataclass(frozen=True, kw_only=True)
class ExportOrgTree:
    by_sap_org_id: Mapping[int, ExportOrg]

    def get(self, key: int) -> ExportOrg | None:
        return self.by_sap_org_id.get(key)

    def __getitem__(self, item: int) -> ExportOrg:
        """Get tree by SAP organization ID."""
        return self.by_sap_org_id[item]

    def __contains__(self, item: int) -> bool:
        """Test if forest contains tree with given SAP organization ID."""
        return item in self.by_sap_org_id

    def __iter__(self) -> Iterator[ExportOrg]:
        seen: set[int] = set()
        for org in self.by_sap_org_id.values():
            if org.sap_org.id not in seen:
                seen.add(org.sap_org.id)
                yield org


class TreeBuilder:
    def __init__(
        self,
        config: CristinMsConfig,
        sap_orgs: Iterable[Orgenhet],
        orgreg_orgs: Iterable[OrgUnit],
        external_validators: Iterable[Callable[[ExportOrg], Any]],
    ) -> None:
        self.config = config

        orgreg_orgs_by_sap_id: dict[int, OrgUnit] = {}
        for ou in orgreg_orgs:
            for org_id in extract_dfo_org_ids(config.orgreg_external_keys, ou):
                orgreg_orgs_by_sap_id[org_id] = ou
        self.orgreg_orgs_by_sap_id = orgreg_orgs_by_sap_id
        self.sap_orgs = SapOrgTree(sap_orgs)
        self.external_validators = external_validators
        self.orgreg_orgs_by_stedkode: dict[Stedkode, OrgUnit] = build_orgreg_by_stedkode(
            self.config.orgreg_external_keys,
            orgreg_orgs,
        )
        self.stedkode_by_sap_org_id: dict[int, Stedkode] = {}
        for org in self.sap_orgs:
            o = org
            while True:
                try:
                    ou = self.orgreg_orgs_by_sap_id[o.id]
                except KeyError:
                    break
                if stedkode := extract_stedkode(self.config.orgreg_external_keys, ou):
                    self.stedkode_by_sap_org_id[org.id] = stedkode
                    break
                if o.parent:
                    o = self.sap_orgs[o.parent.id]
                else:
                    break

    def build(self) -> ExportOrgTree:
        """Build organization tree for export.

        Builds a dictionary with SAP Organisasjon.id as key and
        a tuple of the corresponding SAP org and OrgReg to be exported.

        Certain checks are performed to manipulate the tree.  This is
        configured in CristinMsConfig.  If an org is to be excluded, it is
        removed from the dict.  If it is determined that one should use the
        parent, the key points to the next parent to be exported.
        """
        org_tree: dict[int, ExportOrg] = {}

        def add_org(org_id: int, org: Tree[Orgenhet]) -> None:
            orgreg_org = self.orgreg_orgs_by_sap_id[org.id]
            if org.parent is not None:
                stedkode = extract_stedkode(self.config.orgreg_external_keys, orgreg_org)
                # NTNU's OrgReg tree differs from its DFØ SAP org tree.
                # The SAP tree has more levels, in OrgReg, the external keys (SapOrgId, Stedkode, NSD etc)
                # of the extra sap orgs are collected on the OrgReg leaf nodes.
                # That is, an OrgReg OrgUnit may have multiple SapOrgId, Stedkode etc and
                while org.parent:
                    # Climb the SAP org tree until stedkode != parent stedkode
                    if stedkode != self.stedkode_by_sap_org_id.get(org.parent.id):
                        break
                    org = org.parent
            try:
                self.check_exclude_sap_org(org.data)
            except ExcludedByConfigError as exc:
                logger.info("Excluding %s: %s", org.id, exc)
                return
            export_org = ExportOrg(
                sap_org=org.data,
                orgreg_org=self.orgreg_orgs_by_sap_id[org.id],
            )
            try:
                self.check_external_validators(export_org)
            except ValueError as exc:
                logger.info("Excluding %s due to external validator: %s", org.id, exc)
                return

            org_tree[org_id] = export_org

        def go(org: Tree[Orgenhet]) -> None:
            if org.id in org_tree:
                msg = "This should not happen"
                raise AssertionError(msg)
            try:
                self.check_depth(org)
                self.check_stedkode(org)
            except NoStedkodeError as exc:
                if org.parent is None:
                    logger.info("Stedkode not found for root node %s, continuing", exc)
                    for x in org.children:
                        go(x)
                else:
                    logger.warning("Stedkode not found for %s", exc)
                return
            except LevelTooLarge:
                for x in org:
                    try:
                        # Point this org and all its children to parent
                        if org.parent is None:
                            msg = "Parent is None, this should not happen"
                            raise AssertionError(msg)
                        add_org(x.id, org.parent)
                    except KeyError:
                        pass
                return
            try:
                add_org(org.id, org)
            except KeyError:
                logger.warning("Org %s not found", org.id)
                return
            for x in org.children:
                go(x)

        go(self.sap_orgs.root)
        return ExportOrgTree(by_sap_org_id=org_tree)

    def check_external_validators(self, export_org: ExportOrg) -> None:
        for validator in self.external_validators:
            validator(export_org)

    def check_stedkode(self, org: Tree[Orgenhet]) -> Stedkode:
        try:
            stedkode = extract_stedkode(
                self.config.orgreg_external_keys,
                self.orgreg_orgs_by_sap_id[org.id],
            )
            if stedkode is None:
                raise NoStedkodeError(org.id)
        except KeyError as exc:
            raise NoStedkodeError(org.id) from exc
        else:
            return stedkode

    def is_leaf_node(self, org: Tree[Orgenhet]) -> bool:
        if self.config.org_tree and self.config.org_tree.leaf_nodes:  # noqa: SIM102
            if orgreg_org := self.orgreg_orgs_by_sap_id.get(org.id):  # noqa: SIM102
                if stedkode := extract_stedkode(
                    self.config.orgreg_external_keys,
                    orgreg_org,
                ):
                    return stedkode in self.config.org_tree.leaf_nodes
        return False

    def check_exclude_sap_org(self, org: Orgenhet) -> None:
        config = self.config.org_tree.excluded_sap_orgs
        if config is None:
            return
        #  Bør vi heller bruke orgreg?
        if org.org_kortnavn in config:
            msg = f"{org.org_kortnavn!r} in config.org_tree.excluded_sap_orgs keys"
            raise ExcludedByConfigError(msg)
        if org.organisasjonsnavn in config.values():
            msg = f"{org.organisasjonsnavn!r} in config.org_tree.excluded_sap_orgs values"
            raise ExcludedByConfigError(msg)

    def check_depth(
        self,
        org: Tree[Orgenhet],
    ) -> None:
        """Check whether an org is lower than an institute."""
        config = self.config.org_tree.check_depth
        if config is None:
            return
        depth = org.depth - 1
        if depth > config.maximal_non_root_length or (org.parent and self.is_leaf_node(org.parent)):
            raise LevelTooLarge(depth)
