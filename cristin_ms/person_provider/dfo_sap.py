from __future__ import annotations

import dataclasses
import logging
from typing import TYPE_CHECKING, override

from cristin_ms.frida import AnsettelseItem
from cristin_ms.iga_data import SourceSystem
from cristin_ms.orgreg import extract_stedkode
from cristin_ms.predicate import PredicateContext, negative_results_formatted

if TYPE_CHECKING:
    from collections.abc import Iterator, Mapping

    import abstract_iga_client.iga_result as iga_models
    import dfo_sap_client.models as sap_models
    import orgreg_client.models as orgreg_models

    from cristin_ms.config import CristinMsConfig
    from cristin_ms.context import CristinMsContext
    from cristin_ms.org_tree import ExportOrg


from cristin_ms.frida import PersonItem
from cristin_ms.person_provider.provider import PersonProvider, iga_result_to_iga_data

logger = logging.getLogger(__name__)

__all__ = ["DfoSapPersonProvider"]


@dataclasses.dataclass(frozen=True, kw_only=True, eq=True)
class ExportAnsatt:
    ansatt: sap_models.Ansatt
    stilling: sap_models.Stilling
    innehavere: tuple[sap_models.Innehaver, ...]


class AnsattCollection:
    def __init__(
        self,
        *,
        context: CristinMsContext,
        org_tree: Mapping[int, ExportOrg],
        ansatte_by_id: Mapping[str, sap_models.Ansatt],
        provider: PersonProvider,
    ) -> None:
        self.context = context
        self.provider = provider
        self.org_tree = org_tree
        self.export_ansatte = dict(
            {
                exp.ansatt.id: exp
                for exp in filter(None, map(self._to_export, ansatte_by_id.values()))
            }.items(),
        )

    def _to_export(self, ansatt: sap_models.Ansatt) -> ExportAnsatt | None:
        stilling = self.context.sap_stillinger_by_id.get(ansatt.stilling_id)
        if stilling is None:
            logger.info("Stilling %s not found", ansatt.stilling_id)
            return None

        if ansatt.organisasjon_id not in self.org_tree:
            logger.info(
                "Ansatt %s excluded due to org %s not exported",
                ansatt.id,
                ansatt.organisasjon_id,
            )
            return None

        innehavere = tuple(x for x in stilling.innehaver if x.innehaver_ansattnr == ansatt.id)
        if not innehavere:
            logger.info(
                "Innehaver not found for ansatt %s, stilling %s",
                ansatt.id,
                ansatt.stilling_id,
            )
            return None

        ctx = PredicateContext(
            today=self.context.now.date(),
            get_ansatt=lambda: ansatt,
            get_stilling=lambda: stilling,
            get_iga_result=lambda: self.provider.get_iga_result(ansatt.id),
            get_consents=lambda: self.provider.get_iga_consents(ansatt.id),
        )

        if not self.context.sap_ansatt_filter(ctx):
            if logger.isEnabledFor(logging.INFO):
                # Log all negative results
                logger.info(
                    "Ansatt %s excluded by configured filter. Negative results: %s",
                    ansatt.id,
                    negative_results_formatted(ctx.results),
                )
            return None
        return ExportAnsatt(ansatt=ansatt, stilling=stilling, innehavere=innehavere)

    def __iter__(self) -> Iterator[ExportAnsatt]:
        return iter(self.export_ansatte.values())


def map_ansatt(
    *,
    config: CristinMsConfig,
    iga_result: iga_models.IgaResult | None,
    export_ansatt: ExportAnsatt,
    orgreg_ou: orgreg_models.OrgUnit,
) -> PersonItem:
    ansettelse = map_ansettelse(
        config=config,
        export_ansatt=export_ansatt,
        orgreg_ou=orgreg_ou,
    )
    iga_data = iga_result_to_iga_data(iga_result)
    ansatt = export_ansatt.ansatt
    return PersonItem(
        etternavn=iga_data["last_name"] or ansatt.etternavn,
        fornavn=iga_data["first_names"] or ansatt.fornavn,
        brukernavn=iga_data["username"],
        epost=iga_data["email"] or ansatt.epost,
        ansettelser=[ansettelse] if ansettelse else None,
        fnr=ansatt.fnr,
    )


def map_ansettelse(
    *,
    config: CristinMsConfig,
    export_ansatt: ExportAnsatt,
    orgreg_ou: orgreg_models.OrgUnit,
) -> AnsettelseItem:
    stedkode = extract_stedkode(config.orgreg_external_keys, orgreg_ou)
    if not stedkode:
        msg = "No stedkode for ou %s"
        raise ValueError(msg, orgreg_ou.ou_id)
    ansatt = export_ansatt.ansatt
    stilling = export_ansatt.stilling
    innehaver = export_ansatt.innehavere[0]
    if not innehaver:
        msg = "No innehaver for ansatt %s, stilling %s"
        raise ValueError(msg, ansatt.id, stilling.id)
    return AnsettelseItem(
        institusjonsnr=config.frida.institusjon.institusjonsnr,
        avdnr=stedkode.avdeling,
        undavdnr=stedkode.underavdeling,
        gruppenr=stedkode.gruppe,
        stillingskode=stilling.stillingskode,
        dato_fra=innehaver.innehaver_startdato,
        dato_til=innehaver.innehaver_sluttdato,
        stillingsbetegnelse=stilling.stillingsnavn,
    )


class DfoSapPersonProvider(PersonProvider):
    @override
    @property
    def iga_source_system(self) -> SourceSystem:
        return SourceSystem.SAP

    @override
    def get_person_items(self) -> Iterator[PersonItem]:
        ansatte = AnsattCollection(
            context=self.context.cristin_ms,
            ansatte_by_id=self.context.cristin_ms.sap_ansatte_by_id,
            org_tree=self.context.org_tree.by_sap_org_id,
            provider=self,
        )
        for export_ansatt in ansatte:
            ansatt = export_ansatt.ansatt
            if ansatt.fnr in self.context.exported_persons:
                logger.info(
                    "Ansatt with id %s was already exported",
                    ansatt.id,
                )
                continue
            try:
                orgreg_ou = self.context.org_tree[ansatt.organisasjon_id].orgreg_org
                iga_result = self.context.cristin_ms.get_iga_data(
                    source_system=SourceSystem.SAP,
                    id_=ansatt.id,
                )

                person_item = map_ansatt(
                    config=self.context.cristin_ms.config,
                    export_ansatt=export_ansatt,
                    orgreg_ou=orgreg_ou,
                    iga_result=iga_result,
                )
                self.context.exported_persons.add(person_item.fnr)
                yield person_item
            except ValueError as exc:
                logger.info("Failed to map ansatt %s: %r", ansatt.id, exc)
            except Exception as exc:  # noqa: BLE001
                logger.warning("Failed to map ansatt %s: %r", ansatt.id, exc)
