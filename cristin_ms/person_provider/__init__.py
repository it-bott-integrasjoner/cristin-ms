from collections.abc import Iterable

from cristin_ms.person_provider.dfo_sap import DfoSapPersonProvider
from cristin_ms.person_provider.greg import GregPersonProvider
from cristin_ms.person_provider.provider import PersonProvider, PersonProviderContext

__all__ = [
    "PROVIDERS",
    "PersonProvider",
    "PersonProviderContext",
]

PROVIDERS: Iterable[type[PersonProvider]] = (
    DfoSapPersonProvider,  # This must be first
    GregPersonProvider,
)
