from __future__ import annotations

import abc
import dataclasses
import logging
from typing import TYPE_CHECKING

from cristin_ms.iga_data import IgaData

if TYPE_CHECKING:
    from collections.abc import Iterable, Iterator

    import abstract_iga_client.iga_result as iga_models

    from cristin_ms.context import CristinMsContext
    from cristin_ms.frida import PersonItem
    from cristin_ms.iga_data import SourceSystem
    from cristin_ms.org_tree import ExportOrgTree

__all__ = [
    "PersonProviderContext",
    "PersonProvider",
    "IgaData",
    "iga_result_to_iga_data",
]

logger = logging.getLogger(__name__)


@dataclasses.dataclass(frozen=True, kw_only=True)
class PersonProviderContext:
    cristin_ms: CristinMsContext
    org_tree: ExportOrgTree
    exported_persons: set[str]
    """Set of fødselnummer for exported persons.

    IGA lookup may be expensive
    """


class PersonProvider(abc.ABC):
    def __init__(self, context: PersonProviderContext) -> None:
        self.context = context

    @abc.abstractmethod
    def get_person_items(self) -> Iterator[PersonItem]:
        """Get frida person items."""

    @property
    @abc.abstractmethod
    def iga_source_system(self) -> SourceSystem:
        """Get source system."""

    def get_iga_result(self, id_: str) -> iga_models.IgaResult | None:
        res = self.context.cristin_ms.get_iga_data(
            self.iga_source_system,
            id_,
        )
        if res is None:
            logger.info(
                "IgaResult not found for SourceSystem=%s, id=%s",
                self.iga_source_system,
                id_,
            )
        return res

    def get_iga_consents(self, id_: str) -> Iterable[object]:
        iga_result = self.get_iga_result(id_)
        if iga_result is None:
            return ()
        iga_person_id = iga_result.iga_person_id
        if iga_person_id is None:
            logger.info(
                "IgaResult.iga_person_id is None for SourceSystem=%s, id=%s",
                self.iga_source_system,
                id_,
            )
            return ()
        return self.context.cristin_ms.iga.get_person_consents(iga_person_id) or ()


def iga_result_to_iga_data(iga_person_data: iga_models.IgaResult | None) -> IgaData:
    iga_data = IgaData(
        first_names=None,
        last_name=None,
        email=None,
        username=None,
    )
    if not iga_person_data:
        return iga_data
    iga_display_name = iga_person_data.display_name
    if iga_display_name:
        first_names, last_name = iga_display_name
        if first_names:
            iga_data["first_names"] = first_names
        if last_name:
            iga_data["last_name"] = last_name

    if iga_person_data.email is not None and len(iga_person_data.email) > 0:
        iga_data["email"] = iga_person_data.email
    if iga_person_data.username is not None:
        iga_data["username"] = iga_person_data.username
    return iga_data
