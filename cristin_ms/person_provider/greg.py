from __future__ import annotations

import dataclasses
import logging
from contextlib import suppress
from typing import TYPE_CHECKING, override

import pydantic

from cristin_ms.frida import AnsettelseItem
from cristin_ms.greg_person import extract_ansettelser, extract_greg_person_data
from cristin_ms.iga_data import SourceSystem
from cristin_ms.orgreg import extract_stedkode
from cristin_ms.predicate import (
    PredicateABC,
    PredicateContext,
    negative_results_formatted,
)
from cristin_ms.stedkode import InvalidStedkodeError, Stedkode

if TYPE_CHECKING:
    from collections.abc import Iterator, Mapping

    import greg_client.models as greg_models

    from cristin_ms.config import GuestConfig
    from cristin_ms.context import CristinMsContext
    from cristin_ms.org_tree import ExportOrg


from cristin_ms.frida import PersonItem
from cristin_ms.person_provider.provider import PersonProvider, iga_result_to_iga_data

logger = logging.getLogger(__name__)

__all__ = ["GregPersonProvider"]


@dataclasses.dataclass(frozen=True, kw_only=True, eq=True)
class RoleWithStedkode:
    role: greg_models.Role
    stedkode: Stedkode


@dataclasses.dataclass(frozen=True, kw_only=True, eq=True)
class ExportGregPerson:
    person: greg_models.Person
    roles: tuple[RoleWithStedkode, ...]


class GregPersonCollection:
    def __init__(
        self,
        *,
        context: CristinMsContext,
        org_tree: Mapping[Stedkode, ExportOrg],
        persons_by_id: Mapping[int, greg_models.Person],
        provider: PersonProvider,
    ) -> None:
        guest_config = context.config.guests
        if guest_config is None:
            raise TypeError
        self.provider = provider
        self.guest_config: GuestConfig = guest_config
        self.context = context
        self.org_tree = org_tree
        self.export_persons = dict(
            {
                exp.person.id: exp
                for exp in filter(None, map(self._to_export, persons_by_id.values()))
            }.items(),
        )

    def _to_export(self, person: greg_models.Person) -> ExportGregPerson | None:
        def filter_role(role_with_stedkode: RoleWithStedkode) -> bool:
            return self.context.greg_person_filter(
                PredicateContext(
                    today=self.context.now.date(),
                    get_greg_person=lambda: person,
                    get_greg_role=lambda: role_with_stedkode.role,
                    get_iga_result=lambda: self.provider.get_iga_result(str(person.id)),
                    get_consents=lambda: self.provider.get_iga_consents(str(person.id)),
                    results=filter_results,
                ),
            )

        def add_stedkode(role: greg_models.Role) -> RoleWithStedkode | None:
            try:
                if self.guest_config.stedkode_via_orgreg:
                    stedkode = self.get_stedkode_via_orgreg(role)
                else:
                    stedkode = self.get_stedkode_from_identifier(role)
                return RoleWithStedkode(role=role, stedkode=stedkode)
            except InvalidStedkodeError as exc:
                logger.info("Invalid stedkode %s, Greg person %s, role %s", exc, person.id, role.id)
                return None
            except StopIteration:
                logger.info(
                    "Greg person %s has no stedkode for role %s",
                    person.id,
                    role.id,
                )
                return None

        filter_results: list[tuple[PredicateABC, bool]] = []
        greg_person_data = extract_greg_person_data(person)
        if not greg_person_data.fnr:
            logger.info(
                "Greg person %s has no fnr",
                person.id,
            )
            return None

        if greg_person_data.fnr in self.provider.context.exported_persons:
            logger.info(
                "Greg person %s was already exported",
                person.id,
            )
            return None

        roles = tuple(
            filter(
                filter_role,
                filter(
                    None,
                    map(add_stedkode, extract_ansettelser(person, self.guest_config)),
                ),
            ),
        )
        if not roles:
            if logger.isEnabledFor(logging.INFO):
                if not filter_results:
                    # Predicate was never called
                    logger.info("Greg person %s has no relevant ansettelser", person.id)
                else:
                    # Log all negative results
                    # use dict.fromkeys to remove duplicates
                    logger.info(
                        "Greg person %s excluded by configured filter. Negative results: %s",
                        person.id,
                        list(dict.fromkeys(negative_results_formatted(filter_results))),
                    )
            return None
        return ExportGregPerson(person=person, roles=roles)

    def __iter__(self) -> Iterator[ExportGregPerson]:
        return iter(self.export_persons.values())

    def get_stedkode_from_identifier(self, role: greg_models.Role) -> Stedkode:
        # Grab first relevant stedkode
        return Stedkode(
            next(
                x.value
                for x in role.orgunit.identifiers
                if x.name == self.guest_config.stedkode_identifier_name and x.value in self.org_tree
            ),
        )

    def get_stedkode_via_orgreg(self, role: greg_models.Role) -> Stedkode:
        for identifier in role.orgunit.identifiers:
            if (
                identifier.name == self.guest_config.stedkode_identifier_name
                and identifier.source == self.guest_config.stedkode_identifier_source
            ):
                with suppress(TypeError, ValueError, KeyError):
                    ou_id = int(identifier.value)
                    if org_unit := self.context.orgreg_ous_by_id[ou_id]:  # noqa: SIM102
                        if stedkode := extract_stedkode(
                            self.context.config.orgreg_external_keys,
                            org_unit,
                        ):
                            return stedkode
        raise StopIteration


@dataclasses.dataclass(frozen=True, kw_only=True)
class GregPersonData:
    fnr: str | None
    epost: str | None


class GregPersonProvider(PersonProvider):
    @override
    @property
    def iga_source_system(self) -> SourceSystem:
        return SourceSystem.GREG

    @override
    def get_person_items(self) -> Iterator[PersonItem]:
        if self.context.cristin_ms.config.guests:
            export_org_by_stedkode: dict[Stedkode, ExportOrg] = {
                k: v
                for k, v in {
                    extract_stedkode(
                        config=self.context.cristin_ms.config.orgreg_external_keys,
                        ou=x.orgreg_org,
                    ): x
                    for x in self.context.org_tree
                }.items()
                if k is not None
            }
            greg_persons = GregPersonCollection(
                context=self.context.cristin_ms,
                org_tree=export_org_by_stedkode,
                persons_by_id=self.context.cristin_ms.greg_persons_by_id,
                provider=self,
            )
            for export_greg_person in greg_persons:
                greg_person_data = extract_greg_person_data(export_greg_person.person)
                if greg_person_data.fnr in self.context.exported_persons:
                    logger.info(
                        "Greg person %s was already exported",
                        export_greg_person.person.id,
                    )
                    continue
                try:
                    person_item = self.map_greg_person(
                        export_org_by_stedkode=export_org_by_stedkode,
                        entry=export_greg_person,
                    )
                except pydantic.ValidationError as exc:
                    logger.info("%s", exc)
                else:
                    if person_item is not None:
                        self.context.exported_persons.add(person_item.fnr)
                        yield person_item

    def map_greg_person(
        self,
        *,
        export_org_by_stedkode: dict[Stedkode, ExportOrg],
        entry: ExportGregPerson,
    ) -> PersonItem | None:
        if not self.context.cristin_ms.config.guests:
            raise TypeError
        ansettelser: list[AnsettelseItem] = []
        person = entry.person
        for role_with_stedkode in entry.roles:
            role = role_with_stedkode.role
            stedkode = role_with_stedkode.stedkode
            if stedkode in export_org_by_stedkode:
                role_data = self.context.cristin_ms.config.guests.accepted_role_types[role.type]
                ansettelse = AnsettelseItem(
                    institusjonsnr=self.context.cristin_ms.config.frida.institusjon.institusjonsnr,
                    avdnr=stedkode.avdeling,
                    undavdnr=stedkode.underavdeling,
                    gruppenr=stedkode.gruppe,
                    stillingskode=role_data.stillingskode,
                    dato_fra=role.start_date,
                    dato_til=role.end_date,
                    stillingsbetegnelse=role_data.stillingsbetegnelse,
                )
                ansettelser.append(ansettelse)

        if ansettelser:
            greg_person_data = extract_greg_person_data(person)
            if greg_person_data.fnr:
                iga_result = self.context.cristin_ms.get_iga_data(
                    source_system=SourceSystem.GREG,
                    id_=str(person.id),
                )
                iga_data = iga_result_to_iga_data(iga_result)
                return PersonItem(
                    etternavn=iga_data["last_name"] or person.last_name,
                    fornavn=iga_data["first_names"] or person.first_name,
                    brukernavn=iga_data["username"],
                    epost=iga_data["email"] or greg_person_data.epost,
                    ansettelser=ansettelser,
                    fnr=greg_person_data.fnr,
                )

        logger.info("Greg person %s excluded due to no relevant ansettelser", person.id)
        return None
