"""Everything related to configuration in the project."""

# from __future__ import annotations won't work here because of pydantic
from datetime import time
from http import HTTPStatus
from pathlib import Path
from typing import TYPE_CHECKING, Any

import pydantic
import yaml
from abstract_iga_client.config import ClientConfig as IgaClientConfig
from orgreg_client import ClientConfig
from orgreg_client import models as orgreg_models
from pydantic import (
    ConstrainedInt,
    Extra,
    Field,
    HttpUrl,
    NonNegativeFloat,
    PositiveInt,
    root_validator,
)
from urllib3 import Retry

from cristin_ms import frida
from cristin_ms.predicate.person_query_parser import parse_expression_query
from cristin_ms.stedkode import Stedkode

SERVICE_NAME = __name__.split(".")[0]

CONFIG_ENVIRON = f"{SERVICE_NAME.upper()}_CONFIG"


if TYPE_CHECKING:
    HttpErrorStatus = int
else:

    class HttpErrorStatus(ConstrainedInt):
        ge = 400
        lt = 600


class BaseModel(pydantic.BaseModel):
    class Config:
        extra = Extra.forbid
        allow_mutation = False


# Properties of this class are used to construct a urllib3.Retry object
# See https://urllib3.readthedocs.io/en/stable/reference/urllib3.util.html#urllib3.util.Retry
#
# To print the sleep times in seconds (try different values of total and backoff_factor):
#     total=5; backoff_factor = 10; print([backoff_factor * (2 ** (x - 1)) for x in range(1, total + 1)])  # noqa: ERA001
class RetryConfig(BaseModel):
    total: PositiveInt = 5
    status_forcelist: frozenset[HttpErrorStatus] = frozenset(
        {
            HTTPStatus.UNAUTHORIZED,
            HTTPStatus.INTERNAL_SERVER_ERROR,
            HTTPStatus.BAD_GATEWAY,
            HTTPStatus.SERVICE_UNAVAILABLE,
            HTTPStatus.GATEWAY_TIMEOUT,
        },
    )
    allowed_methods: frozenset[str] = Field(
        frozenset({"POST", "GET", "PUT", "PATCH", "DELETE"}),
        min_items=1,
    )
    backoff_factor: NonNegativeFloat = 2

    @root_validator
    def check_model(cls, values: Any) -> Any:  # noqa: N805
        Retry(**values)
        return values

    def get_urllib3_retry(self) -> Retry:
        return Retry(**self.dict())


class DfoSapClientConfig(BaseModel):
    """Config Class for DfoSapClient."""

    urls: dict[str, str]
    tokens: dict[str, dict[str, str]]
    headers: dict[str, str] | None
    ignore_required_model_fields: bool | None
    match_server_header: bool = True
    stillinger_split_id: PositiveInt | set[PositiveInt] | None
    ansatte_split_id: PositiveInt | set[PositiveInt] | None


class ExpressionFilter(str):
    __slots__ = ()

    @classmethod
    def __get_validators__(cls) -> Any:
        yield cls.validate

    @classmethod
    def validate(cls, value: str) -> str:
        parse_expression_query(value)
        return value


class GregClientConfig(BaseModel):
    """Config Class for GregClient."""

    base_url: str
    headers: dict[str, str] | None


class GuestRole(BaseModel):
    stillingskode: int
    stillingsbetegnelse: str


class GuestConfig(BaseModel):
    greg: GregClientConfig
    # Key: Greg role type
    accepted_role_types: dict[str, GuestRole]
    stedkode_via_orgreg: bool = False
    """When true, identifier is an OrgReg OuId. When false, it's a Stedkode"""
    stedkode_identifier_name: str
    stedkode_identifier_source: str | None
    greg_person_filter: ExpressionFilter


class OrgregExternalKey(BaseModel):
    type: str  # noqa: A003
    source_system: str | None

    def matches(self, other: orgreg_models.ExternalKey) -> bool:
        return self.type == other.type and self.source_system == other.source_system


class OrgregExternalKeyConfig(BaseModel):
    stedkode: OrgregExternalKey
    dfo_org_id: OrgregExternalKey
    nsdkode: OrgregExternalKey


class FridaConfig(BaseModel):
    kilde: str
    institusjon: frida.Institusjon


class CheckOrgTreeDepth(BaseModel):
    maximal_non_root_length: PositiveInt


class OrgTreeConfig(BaseModel):
    check_depth: CheckOrgTreeDepth | None
    leaf_nodes: tuple[Stedkode, ...] = ()
    """These will end up as leaf nodes in the resulting org tree."""
    excluded_sap_orgs: dict[str, str] | None
    """Mapping from SAP org_kortavn to SAP organisasjonsnavn."""


class CristinMsConfig(BaseModel):
    """Cristin MS configuration."""

    sap: DfoSapClientConfig
    frida: FridaConfig
    iga: IgaClientConfig
    orgreg: ClientConfig
    guests: GuestConfig | None
    org_tree: OrgTreeConfig
    orgreg_external_keys: OrgregExternalKeyConfig
    use_orgreg_long_name: bool
    export_time: time
    http_retry: RetryConfig = RetryConfig()
    ansatt_filter: ExpressionFilter


class SentryConfig(BaseModel):
    """Sentry Configuration.

    See https://docs.sentry.io/platforms/python/configuration/options/
    """

    dsn: HttpUrl
    environment: str | None

    class Config:
        # Allow extra keys
        extra = Extra.allow


class LoggingConfig(BaseModel):
    version: int

    class Config:
        # Allow extra keys
        extra = Extra.allow


class CristinMsConfigLoader(BaseModel):
    """Cristin MS configuration loader."""

    cristin_ms: CristinMsConfig
    sentry: SentryConfig | None
    logging: LoggingConfig | None

    @classmethod
    def from_yaml(cls, yamlstr: str) -> "CristinMsConfigLoader":
        """Load from str in yaml format."""
        return cls(**yaml.safe_load(yamlstr))

    @classmethod
    def from_file(cls, filename: Path | str) -> "CristinMsConfigLoader":
        """Load from file."""
        with Path(filename).open() as f:
            return cls.from_yaml(f.read())

    class Config:
        extra = Extra.ignore
