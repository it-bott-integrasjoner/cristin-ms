from __future__ import annotations

__all__ = [
    "InvalidStedkodeError",
    "Stedkode",
]

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Callable, Generator
    from typing import Any

import pydantic.validators


class InvalidStedkodeError(ValueError):
    pass


class Stedkode(str):
    __slots__ = ()

    def __new__(cls, stedkode: str) -> Stedkode:
        if not isinstance(stedkode, str):
            raise TypeError
        _ = int(stedkode)  # Validate int
        if len(stedkode) != 6:  # noqa: PLR2004
            raise InvalidStedkodeError(stedkode)
        return super().__new__(cls, stedkode)

    @classmethod
    def create(cls, *, avdnr: int, undavdnr: int, gruppenr: int) -> Stedkode:
        return Stedkode(f"{avdnr:02d}{undavdnr:02d}{gruppenr:02d}")

    @property
    def avdeling(self) -> int:
        return int(self[:2])

    @property
    def underavdeling(self) -> int:
        return int(self[2:4])

    @property
    def gruppe(self) -> int:
        return int(self[4:])

    @classmethod
    def __get_validators__(cls) -> Generator[Callable[..., Any], None, None]:
        """Get validators.

        Pydantic specific class method so Stedkode may be used as a model field
        """
        # yaml interprets numbers with leading zero as octal, so 012345 turns into 5349
        # Require strict str to avoid surprises
        yield pydantic.validators.strict_str_validator
        # Can't use Stedkode directly, pydantic throws a TypeError if you do
        yield lambda v: Stedkode(v)
