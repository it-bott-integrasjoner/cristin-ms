from __future__ import annotations

import enum
import logging
from typing import TypedDict

from typing_extensions import override

logger = logging.getLogger(__name__)


class IgaData(TypedDict):
    first_names: str | None
    last_name: str | None
    email: str | None
    username: str | None


class SourceSystem(enum.Enum):
    SAP = enum.auto()
    GREG = enum.auto()

    @override
    def __str__(self) -> str:
        return self.name
