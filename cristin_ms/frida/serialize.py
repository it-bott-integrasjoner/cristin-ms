from __future__ import annotations

import datetime
import xml.etree.ElementTree as Et

__all__ = [
    "beskrivelse",
    "institusjon",
    "organisasjon",
    "enhet",
    "personer",
    "ansettelser",
    "person_item",
    "ansettelse_item",
    "frida_import",
]

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from cristin_ms.frida import (
        AnsettelseItem,
        Beskrivelse,
        EnhetItem,
        FridaImport,
        Institusjon,
        PersonItem,
    )


def format_stedkode_part(i: int) -> str:
    return f"{i:02d}"


def beskrivelse(obj: Beskrivelse) -> Et.Element:
    el = Et.Element("beskrivelse")
    text_subelement(el, "kilde", obj.kilde)
    text_subelement(el, "dato", obj.dato)
    return el


def institusjon(obj: Institusjon) -> Et.Element:
    el = Et.Element("institusjon")
    text_subelement(el, "institusjonsnr", obj.institusjonsnr)
    text_subelement(el, "navnBokmal", obj.navn_bokmal)
    text_subelement(el, "navnEngelsk", obj.navn_engelsk)
    text_subelement(el, "akronym", obj.akronym)
    text_subelement(el, "lokalFridaURL", obj.lokal_frida_url)
    text_subelement(el, "lokalFridaEpost", obj.lokal_frida_epost)
    text_subelement(el, "NSDKode", obj.nsd_kode)
    return el


def enhet(obj: EnhetItem) -> Et.Element:
    el = Et.Element("enhet")
    text_subelement(el, "institusjonsnr", obj.institusjonsnr)
    text_subelement(el, "avdnr", format_stedkode_part(obj.avdnr))
    text_subelement(el, "undavdnr", format_stedkode_part(obj.undavdnr))
    text_subelement(el, "gruppenr", format_stedkode_part(obj.gruppenr))
    # Use institusjonsnr for institusjonsnrUnder
    text_subelement(el, "institusjonsnrUnder", format_stedkode_part(obj.institusjonsnr))
    text_subelement(el, "avdnrUnder", format_stedkode_part(obj.avdnr_under))
    text_subelement(el, "undavdnrUnder", format_stedkode_part(obj.undavdnr_under))
    text_subelement(el, "gruppenrUnder", format_stedkode_part(obj.gruppenr_under))
    text_subelement(el, "navnBokmal", obj.navn_bokmal)
    text_subelement(el, "navnEngelsk", obj.navn_engelsk)
    text_subelement(el, "akronym", obj.akronym)
    text_subelement(el, "postadresse", obj.postadresse)
    text_subelement(el, "telefonnr", obj.telefonnr)
    text_subelement(el, "telefaxnr", obj.telefaxnr)
    text_subelement(el, "NSDKode", obj.nsd_kode)
    return el


def organisasjon(obj: list[EnhetItem]) -> Et.Element:
    el = Et.Element("organisasjon")
    for x in obj:
        el.append(enhet(x))
    return el


def ansettelse_item(obj: AnsettelseItem) -> Et.Element:
    el = Et.Element("ansettelse")
    text_subelement(el, "institusjonsnr", obj.institusjonsnr)
    text_subelement(el, "avdnr", format_stedkode_part(obj.avdnr))
    text_subelement(el, "undavdnr", format_stedkode_part(obj.undavdnr))
    text_subelement(el, "gruppenr", format_stedkode_part(obj.gruppenr))
    text_subelement(el, "stillingskode", f"{obj.stillingskode:04d}")
    text_subelement(el, "datoFra", obj.dato_fra)
    text_subelement(el, "datoTil", obj.dato_til)
    text_subelement(el, "stillingsbetegnelse", obj.stillingsbetegnelse)
    return el


def ansettelser(obj: list[AnsettelseItem]) -> Et.Element:
    el = Et.Element("ansettelser")
    for x in obj:
        el.append(ansettelse_item(x))
    return el


def person_item(obj: PersonItem) -> Et.Element:
    el = Et.Element("person", attrib={"fnr": obj.fnr})
    text_subelement(el, "etternavn", obj.etternavn)
    text_subelement(el, "fornavn", obj.fornavn)
    text_subelement(el, "brukernavn", obj.brukernavn)
    text_subelement(el, "epost", obj.epost)
    if obj.ansettelser:
        el.append(ansettelser(obj.ansettelser))
    return el


def personer(obj: list[PersonItem]) -> Et.Element:
    el = Et.Element("personer")
    for x in obj:
        el.append(person_item(x))
    return el


def frida_import(obj: FridaImport) -> Et.Element:
    el = make_root_element()
    el.append(beskrivelse(obj.beskrivelse))
    el.append(institusjon(obj.institusjon))
    el.append(organisasjon(obj.organisasjon))
    el.append(personer(obj.personer))
    return el


def make_root_element() -> Et.Element:
    """Add root element to the Element."""
    return Et.Element(
        "fridaImport",
        {
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
            "xsi:noNamespaceSchemaLocation": "http://frida.usit.uio.no/import/institusjonsdata/schema/Frida-import-1_0.xsd",
        },
    )


def text_subelement(
    parent: Et.Element,
    tag: str,
    value: str | datetime.date | None | int,
) -> None:
    if value is None:
        return
    if isinstance(value, datetime.date):
        value = value.isoformat()
    if isinstance(value, int):
        value = str(value)
    Et.SubElement(parent, tag).text = value
