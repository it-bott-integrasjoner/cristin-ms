# from __future__ import annotations Cannot be because of pydantic

import datetime
import logging
import re
from typing import TYPE_CHECKING, Annotated, Any, NotRequired, TypedDict, override

import pydantic.validators
from pydantic import BaseModel, Extra, Field, validator
from pydantic.fields import ModelField

from cristin_ms.stedkode import Stedkode

if TYPE_CHECKING:
    from collections.abc import Callable, Generator

    CallableGenerator = Generator[Callable[..., Any], None, None]

__all__ = [
    "Beskrivelse",
    "Institusjon",
    "EnhetItem",
    "AnsettelseItem",
    "PersonItem",
    "FridaImport",
    "Brukernavn",
    "Epost",
    "Fornavn",
    "Etternavn",
    "Fnr",
    "Stillingskode",
]

logger = logging.getLogger(__name__)

if TYPE_CHECKING:
    Brukernavn = str
    Epost = str
    Fornavn = str
    Etternavn = str
    Fnr = str
    Max2Digits = int
    Max8Digits = int
    Stillingskode = int
else:

    class Brukernavn(pydantic.ConstrainedStr):
        strict = True
        max_length = 16
        strip_whitespace = True
        to_lower = True

        @classmethod
        @override
        def __get_validators__(cls) -> "CallableGenerator":
            yield pydantic.validators.strict_str_validator
            yield pydantic.validators.constr_strip_whitespace
            yield pydantic.validators.constr_lower
            yield cls.validate

        @classmethod
        @override
        def validate(cls, value: str) -> str | None:
            # Strip feide part from end of usernames
            value = value.split("@", 1)[0]
            if len(value) > cls.max_length:
                return None
            return value

    class Epost(pydantic.ConstrainedStr):
        strict = True
        max_length = 80
        strip_whitespace = True

        @classmethod
        @override
        def __get_validators__(cls) -> "CallableGenerator":
            yield pydantic.validators.strict_str_validator
            yield pydantic.validators.constr_strip_whitespace
            yield pydantic.validators.constr_lower
            yield cls.validate

        @classmethod
        @override
        def validate(cls, value: str) -> str | None:
            # Strip feide part from end of usernames
            value = value.strip()
            if len(value) > cls.max_length:
                return None
            return value

    class Fornavn(pydantic.ConstrainedStr):
        strict = True
        max_length = 30

        @classmethod
        @override
        def __get_validators__(cls) -> "CallableGenerator":
            yield pydantic.validators.strict_str_validator
            yield cls.validate

        @classmethod
        @override
        def validate(cls, value: str) -> str:
            """Reduce until shorter than max length from right."""
            value = value.strip()
            while len(value) > cls.max_length:
                tmp = re.sub(r"\s\S*$", "", value)  # Remove everything after last space
                if tmp == value:  # Give up if too long and can't be made shorter
                    msg = "Unable to shorten value"
                    raise ValueError(msg)
                value = tmp
            return value

    class Etternavn(pydantic.ConstrainedStr):
        strict = True
        max_length = 30

        @classmethod
        @override
        def __get_validators__(cls) -> "CallableGenerator":
            yield pydantic.validators.strict_str_validator
            yield cls.validate

        @classmethod
        @override
        def validate(cls, value: str) -> str:
            """Reduce until shorter than max length from left."""
            value = value.strip()
            while len(value) > cls.max_length:
                tmp = re.sub(r"^.*? ", "", value)  # Remove everything before first space
                if tmp == value:  # Give up if too long and can't be made shorter
                    msg = "Unable to shorten value"
                    raise ValueError(msg)
                value = tmp
            return super().__new__(cls, value)

    class Max8Digits(pydantic.ConstrainedInt):
        ge = 0
        le = 99999999

    class Max2Digits(pydantic.ConstrainedInt):
        ge = 0
        le = 99

    class Stillingskode(pydantic.ConstrainedInt):
        strict = True

        @classmethod
        @override
        def __get_validators__(cls) -> "CallableGenerator":
            yield pydantic.validators.strict_int_validator
            yield cls.validate

        @classmethod
        @override
        def validate(cls, value: int) -> int:
            """Use only last four digits."""
            return int(str(value)[-4:])


class FridaModel(BaseModel):
    class Config:
        extra = Extra.forbid
        allow_population_by_field_name = True
        anystr_strip_whitespace = True
        # Only allow direct instantiation
        allow_mutation = False
        frozen = True


class Beskrivelse(FridaModel):
    kilde: Annotated[str, Field(description="Angivelse av leverandør")]
    dato: Annotated[datetime.date, Field(default_factory=datetime.date.today)]


class Institusjon(FridaModel):
    """Institution.

    This value comes from config, don't massage fields.
    """

    institusjonsnr: Max8Digits
    navn_bokmal: Annotated[str, Field(alias="navnBokmal", max_length=512)]
    navn_engelsk: Annotated[str | None, Field(alias="navnEngelsk", max_length=512)]
    akronym: Annotated[str, Field(max_length=10)]
    lokal_frida_url: Annotated[str | None, Field(alias="lokalFridaURL", max_length=200)]
    lokal_frida_epost: Annotated[str | None, Field(alias="lokalFridaEpost", max_length=80)]
    # XML Schema says totalDigits=8, but max_digits is not enforced by pydantic
    nsd_kode: Annotated[Max8Digits | None, Field(alias="NSDKode")]


class EnhetItem(FridaModel):
    """Orgenhet.

    En enhet er alltid organsert under en enhet. Toppnivå-enhet peker til seg selv.
    """

    institusjonsnr: Max8Digits
    avdnr: Max2Digits
    undavdnr: Max2Digits
    gruppenr: Max2Digits
    avdnr_under: Annotated[Max2Digits, Field(alias="avdnrUnder")]
    undavdnr_under: Annotated[Max2Digits, Field(alias="undavdnrUnder")]
    gruppenr_under: Annotated[Max2Digits, Field(alias="gruppenrUnder")]
    navn_bokmal: Annotated[str, Field(alias="navnBokmal")]
    navn_engelsk: Annotated[str | None, Field(alias="navnEngelsk", max_length=512)]
    akronym: Annotated[str | None, Field(max_length=12)]
    postadresse: Annotated[str | None, Field(description="Gate/postboks", max_length=100)]
    telefonnr: Annotated[str | None, Field(max_length=20)]
    telefaxnr: Annotated[str | None, Field(max_length=20)]
    nsd_kode: Annotated[Max8Digits | None, Field(alias="NSDKode")]

    @property
    def stedkode(self) -> Stedkode:
        return Stedkode.create(
            avdnr=self.avdnr,
            undavdnr=self.undavdnr,
            gruppenr=self.gruppenr,
        )

    @property
    def parent_stedkode(self) -> Stedkode:
        return Stedkode.create(
            avdnr=self.avdnr_under,
            undavdnr=self.undavdnr_under,
            gruppenr=self.gruppenr_under,
        )

    @validator(
        "navn_engelsk",
        "akronym",
        "postadresse",
        "telefaxnr",
        "telefonnr",
        pre=True,
    )
    def check_fields_set_to_none_if_invalid(
        cls,  # noqa: N805
        value: str,
        field: ModelField,
    ) -> Any:
        if not value:
            return None
        if not isinstance(value, str):
            raise TypeError
        value = value.strip()
        try:
            return pydantic.parse_obj_as(field.type_, value)
        except ValueError as exc:
            logger.info("Validation of field %s failed, set to None: %s", field.name, exc)
            return None


class AnsettelseItem(FridaModel):
    institusjonsnr: Max8Digits
    avdnr: Max2Digits
    undavdnr: Max2Digits
    gruppenr: Max2Digits
    stillingskode: Stillingskode
    dato_fra: datetime.date
    dato_til: datetime.date | None
    stillingsbetegnelse: str | None

    @property
    def stedkode(self) -> Stedkode:
        return Stedkode.create(avdnr=self.avdnr, undavdnr=self.undavdnr, gruppenr=self.gruppenr)


class PersonItem(FridaModel):
    etternavn: Etternavn
    fornavn: Fornavn
    brukernavn: Brukernavn | None
    epost: Epost | None
    ansettelser: Annotated[list[AnsettelseItem] | None, Field(min_items=1)]
    fnr: Annotated[
        str,
        Field(
            description="Fødselsnummer - 11 siffer",
            regex="[0-9]{11}",
        ),
    ]


class FridaImportDict(TypedDict):
    beskrivelse: Beskrivelse
    institusjon: Institusjon
    organisasjon: NotRequired[list[EnhetItem]]
    personer: NotRequired[list[PersonItem]]


class FridaImport(FridaModel):
    beskrivelse: Annotated[Beskrivelse, Field(description="Angivelse av leverandør")]
    institusjon: Institusjon
    organisasjon: Annotated[list[EnhetItem], Field(min_items=1)]
    personer: Annotated[list[PersonItem], Field(min_items=1)]

    @pydantic.root_validator
    def check_model(cls, values: FridaImportDict) -> FridaImportDict:  # noqa: N805
        return validate_frida_import(values)


def validate_frida_import(values: FridaImportDict) -> FridaImportDict:
    included_orgs = {x.stedkode: x for x in values.get("organisasjon", {})}
    if not included_orgs:
        msg = "No organisasjon were exported"
        raise ValueError(msg)
    institusjonsnr = values["institusjon"].institusjonsnr
    for org in included_orgs.values():
        if org.parent_stedkode not in included_orgs:
            msg = "missing stedkode %s"
            raise ValueError(msg, org.parent_stedkode)
        if org.institusjonsnr != institusjonsnr:
            msg = "Invalid institusjonsnr %s, expected %s"
            raise ValueError(
                msg,
                org.institusjonsnr,
                institusjonsnr,
            )
    del org
    included_persons = values.get("personer", [])
    if not included_persons:
        msg = "No persons were exported"
        raise ValueError(msg)
    for pers in included_persons:
        for ansettelse in pers.ansettelser or ():
            if ansettelse.stedkode not in included_orgs:
                msg = "Organisasjon %s not found in exported orgs"
                raise ValueError(msg, ansettelse.stedkode)

            if ansettelse.institusjonsnr != institusjonsnr:
                msg = "Invalid institusjonsnr %s, expected %s"
                raise ValueError(
                    msg,
                    ansettelse.institusjonsnr,
                    institusjonsnr,
                )
    del pers
    return values
