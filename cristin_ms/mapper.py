from __future__ import annotations

import logging
from typing import TYPE_CHECKING

import cristin_ms.person_provider
from cristin_ms import frida
from cristin_ms.frida import EnhetItem, PersonItem
from cristin_ms.org_tree import ExportOrg, ExportOrgTree, TreeBuilder
from cristin_ms.orgreg import extract_nsd_koder, extract_stedkode
from cristin_ms.person_provider import PersonProviderContext
from cristin_ms.stedkode import Stedkode

if TYPE_CHECKING:
    from collections.abc import Iterator

    import orgreg_client.models as orgreg_models

    from cristin_ms.config import CristinMsConfig
    from cristin_ms.context import CristinMsContext

logger = logging.getLogger(__name__)


def make_address_str(address: orgreg_models.Address | None) -> str | None:
    if address is None:
        return None
    street = address.street
    postal_code = address.postal_code
    city = address.city
    if street and postal_code and city:
        return street + ", " + postal_code + " " + city
    return None


class Mapper:
    def __init__(self, context: CristinMsContext) -> None:
        self.context = context
        self.export_orgs: ExportOrgTree = TreeBuilder(
            config=context.config,
            sap_orgs=context.sap_orgenheter_by_id.values(),
            orgreg_orgs=context.orgreg_ous_by_id.values(),
            external_validators=[self.validate_export_org],
        ).build()
        """Maps SAP org id to the corresponding SAP and OrgReg orgs to be exported
        """

    @property
    def config(self) -> CristinMsConfig:
        return self.context.config

    def all_enhet_items(self) -> Iterator[EnhetItem]:
        for org in self.export_orgs:
            if enhet := self.map_enhet(org):
                yield enhet

    def all_person_items(self) -> Iterator[PersonItem]:
        exported_persons: set[str] = set()
        provider_context = PersonProviderContext(
            cristin_ms=self.context,
            org_tree=self.export_orgs,
            exported_persons=exported_persons,
        )
        for cls in cristin_ms.person_provider.PROVIDERS:
            yield from cls(context=provider_context).get_person_items()

    def map_enhet(self, exp_org: ExportOrg) -> EnhetItem:
        parent = (
            self.export_orgs.get(exp_org.sap_org.overordn_orgenhet_id)
            if exp_org.sap_org.overordn_orgenhet_id
            else None
        )
        parent_stedkode = extract_stedkode(
            self.config.orgreg_external_keys,
            # Root points to itself
            parent.orgreg_org if parent else exp_org.orgreg_org,
        )
        if parent_stedkode is None:
            msg = "Should have Stedkode by now"
            raise AssertionError(msg)
        return map_enhet_item(
            config=self.config,
            org=exp_org,
            parent_stedkode=parent_stedkode,
        )

    def validate_export_org(self, exp_org: ExportOrg) -> None:
        map_enhet_item(
            config=self.config,
            org=exp_org,
            parent_stedkode=Stedkode("000000"),
        )


def fix_enhet_nsd(nsd_kode: str, *, institusjon: frida.Institusjon) -> str | None:
    if len(nsd_kode) == 10 and nsd_kode.startswith(str(institusjon.nsd_kode)):  # noqa: PLR2004
        return nsd_kode[4:]
    if len(nsd_kode) == 6:  # noqa: PLR2004
        return nsd_kode
    logger.info("Unable to fix NSD kode %r", nsd_kode)
    return None


def map_enhet_item(
    org: ExportOrg,
    *,
    config: CristinMsConfig,
    parent_stedkode: Stedkode,
) -> EnhetItem:
    akronym = org.sap_org.org_kortnavn
    if config.use_orgreg_long_name:
        navn_bokmal = org.orgreg_org.long_name.nob or org.orgreg_org.name.nob
        navn_engelsk = org.orgreg_org.long_name.eng or org.orgreg_org.name.eng
    else:
        navn_bokmal = org.orgreg_org.name.nob
        navn_engelsk = org.orgreg_org.name.eng
    stedkode = extract_stedkode(config.orgreg_external_keys, org.orgreg_org)
    if not stedkode:
        # By now, all orgs should have stedkode
        msg = f"Stedkode not found for orgreg ou {org.orgreg_org.ou_id}"
        raise ValueError(msg)

    nsd_kode: str | None = None
    if nsd_koder := extract_nsd_koder(config, org.orgreg_org):
        if stedkode == parent_stedkode:
            nsd_kode = min((x for x in nsd_koder if len(x) == 4), default=None)  # noqa: PLR2004
        else:
            nsd_kode = min(
                filter(
                    None,
                    (fix_enhet_nsd(x, institusjon=config.frida.institusjon) for x in nsd_koder),
                ),
                default=None,
            )

    return EnhetItem(
        institusjonsnr=config.frida.institusjon.institusjonsnr,
        avdnr=stedkode.avdeling,
        undavdnr=stedkode.underavdeling,
        gruppenr=stedkode.gruppe,
        avdnr_under=parent_stedkode.avdeling,
        undavdnr_under=parent_stedkode.underavdeling,
        gruppenr_under=parent_stedkode.gruppe,
        navn_bokmal=navn_bokmal,
        navn_engelsk=navn_engelsk,
        akronym=akronym,
        nsd_kode=nsd_kode,
        postadresse=make_address_str(org.orgreg_org.postal_address),
        telefaxnr=org.orgreg_org.fax or None,
        telefonnr=org.orgreg_org.phone or None,
    )
