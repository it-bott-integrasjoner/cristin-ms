from __future__ import annotations

import dataclasses
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    import dfo_sap_client.models as sap_models
    import orgreg_client.models as orgreg_models


@dataclasses.dataclass(frozen=True, kw_only=True, eq=True)
class ExportAnsatt:
    ansatt: sap_models.Ansatt
    stilling: sap_models.Stilling
    innehavere: tuple[sap_models.Innehaver, ...]


@dataclasses.dataclass(kw_only=True, frozen=True)
class ExportOrg:
    sap_org: sap_models.Orgenhet
    orgreg_org: orgreg_models.OrgUnit
