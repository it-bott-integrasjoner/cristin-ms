import datetime
import pathlib
from typing import Any, override

import click

from cristin_ms import frida
from cristin_ms.config import CONFIG_ENVIRON, CristinMsConfigLoader
from cristin_ms.context import CristinMsContext
from cristin_ms.exporter import write_xml
from cristin_ms.frida import FridaImport
from cristin_ms.mapper import Mapper


class ConfigParamType(click.ParamType):
    name = "file"

    @override
    def convert(
        self,
        value: Any,
        param: click.Parameter | None,
        ctx: click.Context | None,
    ) -> CristinMsConfigLoader:
        if isinstance(value, CristinMsConfigLoader):
            return value
        try:
            return CristinMsConfigLoader.from_file(pathlib.Path(value))
        except Exception as exc:  # noqa: BLE001
            self.fail(str(exc), param, ctx)


config_option = click.option(
    "-c",
    "--config",
    type=ConfigParamType(),
    envvar=CONFIG_ENVIRON,
    required=True,
)


@click.group
def cli() -> None:
    pass


@cli.command
@config_option
def validate_config(config: CristinMsConfigLoader) -> None:
    if not isinstance(config, CristinMsConfigLoader):
        msg = "unreachable"
        raise TypeError(msg)


@cli.command()
@config_option
@click.option(
    "-o",
    "--output",
    type=click.Path(
        file_okay=True,
        dir_okay=False,
        writable=True,
        path_type=pathlib.Path,
    ),
    help="Output file",
)
@click.option(
    "--indent/--no-indent",
    default=False,
    help="Indent XML",
)
@click.option(
    "--simulate-datetime",
    type=click.DateTime(),
    required=False,
    help="Simulate run at specific date.  May or may not be useful for debugging.",
)
def export(
    config: CristinMsConfigLoader,
    output: pathlib.Path,
    indent: bool,  # noqa: FBT001
    simulate_datetime: datetime.datetime | None,
) -> None:
    mapper = Mapper(context=CristinMsContext(config, now=simulate_datetime))
    enheter = list(mapper.all_enhet_items())
    if not enheter:
        # We want to fail early if no orgs were exported
        msg = "No organization enheter exported"
        raise click.ClickException(msg)
    personer = list(mapper.all_person_items())
    frida_import = FridaImport(
        beskrivelse=frida.Beskrivelse(
            kilde=config.cristin_ms.frida.kilde,
            dato=mapper.context.frida_import_dato,
        ),
        institusjon=config.cristin_ms.frida.institusjon,
        organisasjon=enheter,
        personer=personer,
    )

    schema_path = pathlib.Path(pathlib.Path(__file__).parent / "./Frida-import-1_0.xsd")
    write_xml(
        frida_import,
        output=output,
        schema_path=schema_path,
        indent=indent,
    )


if __name__ == "__main__":  # pragma: no cover
    cli()
