from __future__ import annotations

import contextlib
import logging
from typing import TYPE_CHECKING, TypeVar

from bottint_tree import Tree, build_forest

from cristin_ms.stedkode import Stedkode

if TYPE_CHECKING:
    from collections.abc import Iterable, Iterator

    from orgreg_client import OrgUnit

    from cristin_ms.config import CristinMsConfig, OrgregExternalKeyConfig

    T = TypeVar("T")

logger = logging.getLogger(__name__)


def extract_nsd_koder(config: CristinMsConfig, ou: OrgUnit) -> Iterable[str]:
    return tuple(
        x.value for x in ou.external_keys if config.orgreg_external_keys.nsdkode.matches(x)
    )


def extract_stedkode(config: OrgregExternalKeyConfig, ou: OrgUnit) -> Stedkode | None:
    stedkode = min(
        (x.value[-6:] for x in ou.external_keys if config.stedkode.matches(x)),
        default=None,
    )
    return Stedkode(stedkode) if stedkode else None


def extract_stedkoder(config: OrgregExternalKeyConfig, ou: OrgUnit) -> tuple[Stedkode, ...]:
    def stedkoder() -> Iterator[Stedkode]:
        for key in ou.external_keys:
            if config.stedkode.matches(key):
                with contextlib.suppress(ValueError):
                    yield Stedkode(key.value[-6:])

    return tuple(stedkoder())


def extract_dfo_org_ids(config: OrgregExternalKeyConfig, ou: OrgUnit) -> tuple[int, ...]:
    def org_ids() -> Iterator[int]:
        for key in ou.external_keys:
            if config.dfo_org_id.matches(key):
                with contextlib.suppress(ValueError):
                    yield int(key.value)

    return tuple(org_ids())


def build_orgreg_by_stedkode(
    config: OrgregExternalKeyConfig,
    ous: Iterable[OrgUnit],
) -> dict[Stedkode, OrgUnit]:
    orgreg_orgs_by_stedkode: dict[Stedkode, Tree[OrgUnit]] = {}
    orgreg_tree: dict[int, Tree[OrgUnit]] = build_forest(
        ous,
        id_key=lambda x: x.ou_id,
        parent_key=lambda x: x.parent,
        check_parent=False,
    )
    for ou_node in orgreg_tree.values():
        if stedkode := extract_stedkode(config, ou_node.data):
            if stedkode in orgreg_orgs_by_stedkode:
                logger.info("Multiple OrgReg orgs with the same stedkode %s", stedkode)
                stored_node = orgreg_orgs_by_stedkode[stedkode]
                if is_on_same_branch(ou_node, stored_node):
                    if ou_node.depth > stored_node.depth:
                        # The previously stored node is a parent of ou_node. Keep it
                        continue
                else:
                    logger.warning(
                        "Multiple OrgReg orgs with the same stedkode %s, but not on the same branch",
                        stedkode,
                    )
                    # What to do?
                    continue
            orgreg_orgs_by_stedkode[stedkode] = ou_node
    return {k: v.data for k, v in orgreg_orgs_by_stedkode.items()}


def is_on_same_branch(a: Tree[T], b: Tree[T]) -> bool:
    if a.depth == b.depth:
        return a is b
    if a.depth > b.depth:
        return is_on_same_branch(b, a)
    # Now that we know b.depth > a.depth
    # the problem is reduced to: Is a parent of b?
    if not b.parent:
        return False
    return is_on_same_branch(a, b.parent)
