from __future__ import annotations

import logging
import xml.etree.ElementTree
from typing import TYPE_CHECKING
from xml.etree.ElementTree import ElementTree

import xmlschema

from cristin_ms.frida import FridaImport, serialize_frida_import

if TYPE_CHECKING:
    import pathlib

logger = logging.getLogger(__name__)


def write_xml(
    data: FridaImport,
    /,
    *,
    output: pathlib.Path,
    schema_path: pathlib.Path,
    indent: bool = False,
) -> None:
    logger.info(
        "Writing frida import to %s, %s organizations, %s persons",
        output.absolute(),
        len(data.organisasjon),
        len(data.personer),
    )
    tree = ElementTree(serialize_frida_import(data))
    if indent:
        xml.etree.ElementTree.indent(tree)
    tree.write(
        output,
        encoding="ISO-8859-1",
    )
    schema = xmlschema.XMLSchema(schema_path)
    logger.info("Validating Cristin import with schema %s", schema_path.absolute())
    schema.validate(output)
