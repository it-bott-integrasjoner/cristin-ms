from __future__ import annotations

import logging
from http import HTTPStatus
from typing import TYPE_CHECKING
from urllib.parse import parse_qs, urlparse

import greg_client
import greg_client.api.api.api_v1_persons_list as persons_list_api
from greg_client.types import UNSET, Unset

if TYPE_CHECKING:
    from collections.abc import Iterator

    from greg_client.models import PaginatedPersonList
    from greg_client.models import Person as GregPerson

logger = logging.getLogger(__name__)


class GregError(Exception):
    pass


def get_greg_person_list(
    client: greg_client.Client,
    *,
    cursor: str | Unset = UNSET,
) -> PaginatedPersonList | None:
    guest_data_raw = persons_list_api.sync_detailed(
        client=client,  # type: ignore[arg-type]
        cursor=cursor,
    )
    if guest_data_raw.status_code != HTTPStatus.OK:
        msg = f"Got response with status code {guest_data_raw.status_code} and content {guest_data_raw.content!r}"
        raise GregError(
            msg,
        )
    return guest_data_raw.parsed


def iterate_greg_persons(
    client: greg_client.Client,
    persons: PaginatedPersonList,
) -> Iterator[GregPerson]:
    seen_cursors = set()
    optional_persons: PaginatedPersonList | None = persons
    while optional_persons and optional_persons.results:
        yield from optional_persons.results
        if not isinstance(optional_persons.next_, str):
            return
        uri = urlparse(optional_persons.next_)
        query_params = parse_qs(uri.query)
        cursor = query_params["cursor"][0]
        if cursor in seen_cursors:
            # Simple sanity check to prevent potential infinite loop
            logger.warning("Cursor %s already fetched", cursor)
            return
        seen_cursors.add(cursor)
        optional_persons = get_greg_person_list(client=client, cursor=cursor)


def get_guest_employees(client: greg_client.Client) -> Iterator[GregPerson]:
    guest_data_raw = get_greg_person_list(client)
    if not guest_data_raw or not guest_data_raw.results:
        return

    yield from iterate_greg_persons(client, guest_data_raw)
