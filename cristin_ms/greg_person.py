from __future__ import annotations

import dataclasses
import logging
from typing import TYPE_CHECKING

import greg_client.models as greg_models

if TYPE_CHECKING:
    from collections.abc import Iterator

    from cristin_ms.config import GuestConfig

logger = logging.getLogger(__name__)


@dataclasses.dataclass(frozen=True, kw_only=True)
class GregPersonData:
    fnr: str | None
    epost: str | None


def extract_ansettelser(
    person: greg_models.Person,
    config: GuestConfig,
) -> Iterator[greg_models.Role]:
    yield from (x for x in person.roles if x.type in config.accepted_role_types)


def extract_greg_person_data(person: greg_models.Person) -> GregPersonData:
    fnr: str | None = None
    epost: str | None = None
    for subentry in person.identities:
        if subentry.type == greg_models.TypeEnum.NORWEGIAN_NATIONAL_ID_NUMBER:
            fnr = subentry.value
        elif subentry.type == greg_models.TypeEnum.FEIDE_EMAIL:
            epost = subentry.value
    return GregPersonData(fnr=fnr, epost=epost)
