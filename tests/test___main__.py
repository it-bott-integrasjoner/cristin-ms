import json

import pytest
import yaml
from click.testing import CliRunner

import cristin_ms.__main__ as main


@pytest.fixture()
def cli_runner(config_loader, tmp_config_path):
    with tmp_config_path.open("w", encoding="utf-8") as f:
        yaml.safe_dump(json.loads(config_loader.json()), f)
    return CliRunner(mix_stderr=False)


@pytest.fixture()
def tmp_config_path(tmp_path):
    return tmp_path / "cristin_ms_test.yaml"


def test_config_param_type_config_loader(config_loader):
    cl = main.ConfigParamType().convert(config_loader, param=None, ctx=None)

    assert cl is config_loader


def test_cli_validate_config(cli_runner, tmp_config_path):
    result = cli_runner.invoke(
        main.cli,
        args=["validate-config", "--config", str(tmp_config_path)],
        env={},
        catch_exceptions=False,
    )
    assert result.exit_code == 0


def test_cli_validate_config_invalid_config(cli_runner, tmp_path):
    tmp_config_path = tmp_path / "config.yaml"
    with tmp_config_path.open("w") as f:
        yaml.safe_dump({}, f)
    result = cli_runner.invoke(
        main.cli,
        args=["validate-config", "--config", str(tmp_config_path)],
        env={},
        catch_exceptions=False,
    )
    assert result.exit_code == 2
    assert "validation error for CristinMsConfigLoader" in result.stderr
