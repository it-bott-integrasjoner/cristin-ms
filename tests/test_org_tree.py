from cristin_ms.org_tree import TreeBuilder


def test_tree_builder(
    cristin_ms_config1_use_orgreg_long_name_true,
    orgreg_orgunit_objects,
    organisasjoner_objects,
):
    builder = TreeBuilder(
        config=cristin_ms_config1_use_orgreg_long_name_true,
        sap_orgs=organisasjoner_objects,
        orgreg_orgs=orgreg_orgunit_objects,
        external_validators=(),
    )

    tree = builder.build()
    assert set(tree.by_sap_org_id.keys()) == {10000003, 10000011, 10000014, 10000017, 10000018}
    assert tree[10000003].sap_org.id == 10000003
    assert tree[10000011].sap_org.id == 10000011
    assert tree[10000014].sap_org.id == 10000014
    assert tree[10000017].sap_org.id == 10000017
    assert tree[10000018].sap_org.id == 10000017
