import datetime
import logging

import greg_client.models as greg_models
import pytest
from cerebrum_client.models import Consent
from greg_client.types import UNSET

from cristin_ms.predicate import ObjectTag, PredicateContext
from cristin_ms.predicate.exceptions import DateOutOfRangeError
from cristin_ms.predicate.functions import (
    DAYS_AFTER,
    DAYS_BEFORE,
    HasCerebrumConsent,
    IsEmployed,
    IsFnrValid,
    check_dates_in_range,
)


@pytest.mark.parametrize(
    ("date", "startdato", "sluttdato", "days_before", "days_after"),
    [
        (
            datetime.date(2000, 1, 1),
            datetime.date(2000, 1, 1),
            datetime.date(2000, 1, 1),
            0,
            0,
        ),
        (
            datetime.date(2000, 1, 1),
            datetime.date(2000, 1, 1),
            datetime.date(2000, 1, 1),
            14,
            3,
        ),
    ],
)
def test_check_dates_in_range_ok(
    date,
    startdato,
    sluttdato,
    days_before,
    days_after,
):
    check_dates_in_range(
        date=date,
        startdato=startdato,
        sluttdato=sluttdato,
        days_before=days_before,
        days_after=days_after,
    )


@pytest.mark.parametrize(
    ("date", "startdato", "sluttdato", "days_before", "days_after", "match"),
    [
        (
            datetime.date(2000, 1, 1),
            datetime.date(2000, 1, 1),
            datetime.date(1999, 1, 1),
            0,
            0,
            r"It's more than \d+ days since sluttdato",
        ),
        (
            datetime.date(2000, 1, 1),
            datetime.date(2001, 1, 1),
            datetime.date(2000, 1, 1),
            0,
            0,
            r"It's more than \d+ days until startdato",
        ),
        (datetime.date(2000, 1, 1), None, datetime.date(2000, 1, 1), 0, 0, r"startdato is nullish"),
        (
            datetime.date(2000, 1, 1),
            UNSET,
            datetime.date(2000, 1, 1),
            0,
            0,
            r"startdato is nullish",
        ),
    ],
)
def test_check_dates_in_range_not_ok(
    date,
    startdato,
    sluttdato,
    days_before,
    days_after,
    match,
):
    with pytest.raises(DateOutOfRangeError, match=match):
        check_dates_in_range(
            date=date,
            startdato=startdato,
            sluttdato=sluttdato,
            days_before=days_before,
            days_after=days_after,
        )


@pytest.mark.parametrize(
    ("consents", "expected"),
    [
        ([], False),
        (
            [
                Consent(type="opt-in", name="cristin", description="description"),
            ],
            True,
        ),
        (
            [
                Consent(type="opt-in", name="cristin", description="description"),
                Consent(type="opt-in", name="n", description="description"),
            ],
            True,
        ),
        (
            [
                Consent(type="opt-in", name="n", description="description"),
                Consent(type="opt-in", name="cristin", description="description"),
            ],
            True,
        ),
        (
            [
                Consent(type="opt-in", name="n", description="description"),
            ],
            False,
        ),
        (
            [
                Consent(type="t", name="cristin", description="description"),
            ],
            False,
        ),
    ],
)
def test_has_cerebrum_consent(consents, expected):
    ctx = PredicateContext(today=datetime.date.today(), get_consents=lambda: consents)
    p = HasCerebrumConsent(name="cristin", type_="opt-in")

    res = p(ctx)

    assert res == expected


@pytest.mark.parametrize(
    ("sluttdato_timedelta_days", "expected"),
    [
        (0, True),
        (1, True),
        (-DAYS_AFTER, True),
        (-(DAYS_AFTER + 1), False),
    ],
)
def test_is_employed_ansatt_sluttdato(ansatt_00101219, sluttdato_timedelta_days, expected):
    ansatt_00101219.startdato = datetime.date.today()
    ansatt_00101219.sluttdato = ansatt_00101219.startdato + datetime.timedelta(
        days=sluttdato_timedelta_days,
    )
    ctx = PredicateContext(today=datetime.date.today(), get_ansatt=lambda: ansatt_00101219)
    p = IsEmployed(tag=ObjectTag.ANSATT)

    res = p(ctx)

    assert res == expected


@pytest.mark.parametrize(
    ("startdato_timedelta_days", "expected"),
    [
        (0, True),
        (1, True),
        (DAYS_BEFORE, True),
        (DAYS_BEFORE + 1, False),
    ],
)
def test_is_employed_ansatt_startdato(ansatt_00101219, startdato_timedelta_days, expected):
    ansatt_00101219.sluttdato = datetime.date.today()
    ansatt_00101219.startdato = ansatt_00101219.sluttdato + datetime.timedelta(
        days=startdato_timedelta_days,
    )
    ctx = PredicateContext(today=datetime.date.today(), get_ansatt=lambda: ansatt_00101219)
    p = IsEmployed(tag=ObjectTag.ANSATT)

    res = p(ctx)

    assert res == expected


@pytest.mark.parametrize(
    ("sluttdato_timedelta_days", "expected"),
    [
        (0, True),
        (1, True),
        (-DAYS_AFTER, True),
        (-(DAYS_AFTER + 1), False),
    ],
)
def test_is_employed_greg_role_sluttdato(greg_list_objects, sluttdato_timedelta_days, expected):
    today = datetime.date.today()
    greg_role = greg_list_objects[0].roles[0]
    greg_role.start_date = today
    greg_role.end_date = today + datetime.timedelta(days=sluttdato_timedelta_days)
    ctx = PredicateContext(today=today, get_greg_role=lambda: greg_role)
    p = IsEmployed(tag=ObjectTag.GREG_ROLE)

    res = p(ctx)

    assert res == expected


@pytest.mark.parametrize(
    ("startdato_timedelta_days", "expected"),
    [
        (0, True),
        (1, True),
        (DAYS_BEFORE, True),
        (DAYS_BEFORE + 1, False),
    ],
)
def test_is_employed_greg_role_startdato(
    greg_list_objects,
    caplog,
    startdato_timedelta_days,
    expected,
):
    caplog.set_level(logging.NOTSET)
    today = datetime.date.today()
    greg_role = greg_list_objects[0].roles[0]
    greg_role.start_date = today + datetime.timedelta(days=startdato_timedelta_days)
    greg_role.end_date = today
    ctx = PredicateContext(today=today, get_greg_role=lambda: greg_role)
    p = IsEmployed(tag=ObjectTag.GREG_ROLE)

    res = p(ctx)

    assert res == expected


@pytest.mark.parametrize(
    "object_tag",
    [
        ObjectTag.ANSATT,
        ObjectTag.GREG_ROLE,
    ],
)
def test_is_employed_raises_type_error_on_no_object(object_tag):
    ctx = PredicateContext(today=datetime.date.today())
    p = IsEmployed(tag=object_tag)

    with pytest.raises(TypeError, match=""):
        p(ctx)


def test_is_employed_unsupported_object_tag():
    ctx = PredicateContext(today=datetime.date.today())
    p = IsEmployed(tag=ObjectTag.IGA_RESULT)

    with pytest.raises(ValueError, match="not supported"):
        p(ctx)


@pytest.mark.parametrize(
    ("fnr", "expected"),
    [
        ("01010100000", False),
        (None, False),
        # Generert https://norske-testdata.no/fnr/
        ("22059002035", True),
    ],
)
def test_is_fnr_valid(ansatt_00101219, fnr, expected):
    ansatt_00101219.fnr = fnr
    ctx = PredicateContext(today=datetime.date.today(), get_ansatt=lambda: ansatt_00101219)
    p = IsFnrValid(tag=ObjectTag.ANSATT)
    res = p(ctx)

    assert res == expected


@pytest.mark.parametrize(
    ("fnr", "expected"),
    [
        ("01010100000", False),
        (None, False),
        # Generert https://norske-testdata.no/fnr/
        ("22059002035", True),
    ],
)
def test_is_fnr_greg_person(greg_list_objects, fnr, expected):
    greg_person = greg_list_objects[0]
    greg_person.identities = [
        greg_models.Identity(
            id=1,
            created=datetime.datetime.now(),
            updated=datetime.datetime.now(),
            type=greg_models.TypeEnum.NORWEGIAN_NATIONAL_ID_NUMBER,
            source="ANY",
            value=fnr,
            person=greg_person.id,
        ),
    ]
    ctx = PredicateContext(today=datetime.date.today(), get_greg_person=lambda: greg_person)
    p = IsFnrValid(tag=ObjectTag.GREG_PERSON)
    res = p(ctx)

    assert res == expected


@pytest.mark.parametrize(
    "object_tag",
    [
        ObjectTag.ANSATT,
        ObjectTag.GREG_PERSON,
    ],
)
def test_is_fnr_valid_ansatt_raises_type_error_on_no_object(object_tag):
    ctx = PredicateContext(today=datetime.date.today())
    p = IsFnrValid(tag=object_tag)

    with pytest.raises(TypeError, match=""):
        p(ctx)


def test_is_fnr_valid_unsupported_object_tag():
    ctx = PredicateContext(today=datetime.date.today())
    p = IsFnrValid(tag=ObjectTag.IGA_RESULT)

    with pytest.raises(ValueError, match="not supported"):
        p(ctx)
