import pytest
from dfo_sap_client.models import Orgenhet
from orgreg_client import OrgUnit

from cristin_ms import frida
from cristin_ms.mapper import map_enhet_item
from cristin_ms.stedkode import Stedkode
from cristin_ms.types import ExportOrg


@pytest.fixture()
def orgreg_eiendom():
    return OrgUnit(
        ou_id=1000001,
        valid_from="2000-01-01",
        postal_address={
            "postalCode": "5020",
            "city": "Bergen",
            "street": "Sydneshaugen skole",
            "country": "no",
        },
        phone="+4755589400",
        fax="+4755589403",
        name={"nob": "OrgReg EIA - Driftsområde 1", "eng": "OrgReg EIA - Area 1"},
        long_name={
            "nob": "OrgReg Den lange Eiendomsavdelingen - Driftsområde 1",
            "eng": "OrgReg The long property department - Area 1",
        },
        external_keys=[
            {"type": "dfo_org_id", "value": "10000007", "sourceSystem": "dfo_sap"},
            {"type": "legacy_stedkode", "value": "231010"},
            {"type": "nsd", "value": "123456"},
        ],
    )


@pytest.fixture()
def sap_eiendom():
    return Orgenhet(
        id=10000007,
        org_kortnavn="SAP kortnavn",
        organisasjonsnavn="SAP Eiendomsavd",
        overordn_orgenhet_id=10000006,
    )


def test_map_enhet_item_use_orgreg_long_name_false(
    cristin_ms_config1_use_orgreg_long_name_false,
    sap_eiendom,
    orgreg_eiendom,
):
    config = cristin_ms_config1_use_orgreg_long_name_false
    expected = frida.EnhetItem(
        institusjonsnr=404,
        avdnr=23,
        undavdnr=10,
        gruppenr=10,
        avdnr_under=23,
        undavdnr_under=10,
        gruppenr_under=0,
        navn_bokmal="OrgReg EIA - Driftsområde 1",
        navn_engelsk="OrgReg EIA - Area 1",
        akronym="SAP kortnavn",
        postadresse="Sydneshaugen skole, 5020 Bergen",
        telefonnr="+4755589400",
        telefaxnr="+4755589403",
        nsd_kode=123456,
    )

    mapped = map_enhet_item(
        config=config,
        org=ExportOrg(sap_org=sap_eiendom, orgreg_org=orgreg_eiendom),
        parent_stedkode=Stedkode("231000"),
    )

    assert mapped == expected


def test_map_enhet_item_use_orgreg_long_name_true(
    cristin_ms_config1_use_orgreg_long_name_true,
    sap_eiendom,
    orgreg_eiendom,
):
    config = cristin_ms_config1_use_orgreg_long_name_true
    expected = frida.EnhetItem(
        institusjonsnr=404,
        avdnr=23,
        undavdnr=10,
        gruppenr=10,
        avdnr_under=23,
        undavdnr_under=10,
        gruppenr_under=0,
        navn_bokmal="OrgReg Den lange Eiendomsavdelingen - Driftsområde 1",
        navn_engelsk="OrgReg The long property department - Area 1",
        akronym="SAP kortnavn",
        postadresse="Sydneshaugen skole, 5020 Bergen",
        telefonnr="+4755589400",
        telefaxnr="+4755589403",
        nsd_kode=123456,
    )

    mapped = map_enhet_item(
        config=config,
        org=ExportOrg(sap_org=sap_eiendom, orgreg_org=orgreg_eiendom),
        parent_stedkode=Stedkode("231000"),
    )

    assert mapped == expected
