import datetime
import json
from typing import TYPE_CHECKING

import pytest
from abstract_iga_client.iga_result import IgaResult
from dfo_sap_client.models import Ansatt

from cristin_ms.predicate import (
    ObjectTag,
    PredicateContext,
)
from cristin_ms.predicate.get_value import BinaryOperator, Expression, GetValue, Path
from cristin_ms.predicate.person_query_parser import parse_expression_query

if TYPE_CHECKING:
    import pydantic


@pytest.fixture()
def today():
    return datetime.date(
        2023,
        1,
        1,
    )


@pytest.fixture()
def object_properties(ansatt_00101219, iga_all_employees):
    object_: pydantic.BaseModel = ansatt_00101219
    klass = object_.__class__
    values = [(object_, x) for x in klass.__fields__]

    object_ = iga_all_employees[0]
    klass = object_.__class__
    values += [(object_, x) for x in klass.__fields__]
    return values


def test_property_equals_object_ok(
    object_properties,
    today,
):
    for object_, property_name in object_properties:
        if isinstance(object_, Ansatt):
            tag = ObjectTag.ANSATT
            qc = PredicateContext(
                today=today,
                get_ansatt=lambda x=object_: x,  # type: ignore[misc]
            )
        elif isinstance(object_, IgaResult):
            tag = ObjectTag.IGA_RESULT
            qc = PredicateContext(
                today=today,
                get_iga_result=lambda x=object_: x,  # type: ignore[misc]
            )
        else:
            raise TypeError

        # Convert to json to test on string representation of value
        object_dict = json.loads(object_.json(by_alias=False))
        pe = Expression(
            a=GetValue(tag=tag, path=Path(keys=(property_name,))),
            b=object_dict[property_name],
            operator=BinaryOperator.EQ,
        )
        assert pe(qc)


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        (("1",), True),
        (("4",), False),
        (("1", "4"), True),
        (("4", "1"), True),
    ],
)
def test_property_contains_object_ok(
    value,
    expected,
    today,
    ansatt_00101219,
):
    object_ = ansatt_00101219
    property_name = "medarbeidergruppe"
    qc = PredicateContext(
        today=today,
        get_ansatt=lambda: object_,
    )

    pe = Expression(
        a=GetValue(tag=ObjectTag.ANSATT, path=Path(keys=(property_name,))),
        b=value,
        operator=BinaryOperator.IN,
    )

    assert pe(qc) == expected


def test_check_emeritus(
    today,
    ansatt_00101219,
):
    object_ = ansatt_00101219
    object_.medarbeidergruppe = "9"
    object_.medarbeiderundergruppe = "93"

    ctx = PredicateContext(
        today=today,
        get_ansatt=lambda: object_,
    )

    is_emeritus = parse_expression_query(
        'Ansatt::medarbeidergruppe = "9" && Ansatt::medarbeiderundergruppe = "93"',
    )
    assert is_emeritus(ctx)


def test_check_ansatt_and_iga_result(
    today,
    ansatt_00101219,
    iga_all_employees,
):
    object_ = ansatt_00101219
    object_.medarbeidergruppe = "9"
    object_.medarbeiderundergruppe = "93"
    iga_result = iga_all_employees[0]

    ctx = PredicateContext(
        today=today,
        get_ansatt=lambda: object_,
        get_iga_result=lambda: iga_all_employees[0],
    )

    pred = parse_expression_query(
        f"""Ansatt::medarbeidergruppe = {object_.medarbeidergruppe!r}
            && Ansatt::startdato >= '1099-01-01'
            && Ansatt::startdato <= '2099-01-01'
            && IgaResult::sap_person_id = {iga_result.sap_person_id!r}
            && IgaResult::greg_person_id = null
        """,
    )
    assert pred(ctx)


def test_predicate_context_get_value_ansatt(ansatt_00101219):
    today = datetime.date.today()
    ctx = PredicateContext(today=today, get_ansatt=lambda: ansatt_00101219)
    value = ctx.get_value(ObjectTag.ANSATT)

    assert value is ansatt_00101219


def test_predicate_context_get_value_greg_person(greg_list_objects):
    greg_person = greg_list_objects[0]
    today = datetime.date.today()
    ctx = PredicateContext(today=today, get_greg_person=lambda: greg_person)
    value = ctx.get_value(ObjectTag.GREG_PERSON)

    assert value is greg_person


def test_predicate_context_get_value_greg_role(greg_list_objects):
    greg_role = greg_list_objects[0].roles[0]
    today = datetime.date.today()
    ctx = PredicateContext(today=today, get_greg_role=lambda: greg_role)
    value = ctx.get_value(ObjectTag.GREG_ROLE)

    assert value is greg_role


def test_predicate_context_get_value_iga_result(iga_olgaboe):
    today = datetime.date.today()
    ctx = PredicateContext(today=today, get_iga_result=lambda: iga_olgaboe)
    value = ctx.get_value(ObjectTag.IGA_RESULT)

    assert value is iga_olgaboe


def test_predicate_context_get_value_stilling(stilling_30045705_obj):
    today = datetime.date.today()
    ctx = PredicateContext(today=today, get_stilling=lambda: stilling_30045705_obj)
    value = ctx.get_value(ObjectTag.STILLING)

    assert value is stilling_30045705_obj


def test_predicate_context_get_value_innehaver(stilling_30045705_obj):
    innehaver = stilling_30045705_obj.innehaver[0]
    today = datetime.date.today()
    ctx = PredicateContext(today=today, get_innehaver=lambda: innehaver)
    value = ctx.get_value(ObjectTag.INNEHAVER)

    assert value is innehaver
