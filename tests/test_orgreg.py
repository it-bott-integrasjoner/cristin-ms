import bottint_tree
import pytest

from cristin_ms.orgreg import build_orgreg_by_stedkode, is_on_same_branch
from cristin_ms.stedkode import Stedkode


@pytest.mark.parametrize(
    ("a", "b", "expected"),
    [
        (1, 1, True),
        (1, 2, True),
        (2, 3, False),
        (3, 4, True),
        (2, 4, False),
    ],
)
def test_is_on_same_branch(a, b, expected):
    forest = bottint_tree.build_forest(
        [
            {"id": 1, "parent": None},
            {"id": 2, "parent": 1},
            {"id": 3, "parent": 1},
            {"id": 4, "parent": 3},
        ],
        lambda x: x["id"],  # type: ignore[index]
        lambda x: x["parent"],  # type: ignore[index]
    )
    assert is_on_same_branch(forest[a], forest[b]) is expected
    assert is_on_same_branch(forest[b], forest[a]) is expected


def test_build_orgreg_by_stedkode(orgreg_orgunit_objects, config_loader):
    res = build_orgreg_by_stedkode(
        config_loader.cristin_ms.orgreg_external_keys,
        orgreg_orgunit_objects,
    )
    assert res[Stedkode("800000")].ou_id == 1000003
