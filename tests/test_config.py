from __future__ import annotations

import os

import pytest
from urllib3 import Retry

from cristin_ms.config import CristinMsConfig, CristinMsConfigLoader, RetryConfig


@pytest.fixture()
def example_config_path():
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    return os.path.join(here, "..", "config.example.yaml")


def test_config_loader(example_config_path):
    with open(example_config_path) as f:
        example_config_yamlstr = f.read()
    config_from_yamlstr = CristinMsConfigLoader.from_yaml(example_config_yamlstr)
    config_from_file = CristinMsConfigLoader.from_file(example_config_path)
    assert config_from_file == config_from_yamlstr
    assert isinstance(config_from_file, CristinMsConfigLoader)
    assert isinstance(config_from_file.cristin_ms, CristinMsConfig)


def test_retry_config_get_urllib3_retry():
    config = RetryConfig()
    retry = config.get_urllib3_retry()

    assert isinstance(retry, Retry)
