from __future__ import annotations

import json
import pathlib

import greg_client.models
import pytest
import yaml
from abstract_iga_client.iga_result import IgaResult
from dfo_sap_client.models import Ansatt, Orgenhet, Stilling
from orgreg_client import OrgUnit

from cristin_ms.config import CristinMsConfigLoader


@pytest.fixture()
def cristin_ms_config1_yaml():
    return load_yaml_file("config1.yaml")


@pytest.fixture()
def cristin_ms_config1_use_orgreg_long_name_false(cristin_ms_config1_yaml):
    config = CristinMsConfigLoader(**cristin_ms_config1_yaml).cristin_ms
    return config.copy(update={"use_orgreg_long_name": False})


@pytest.fixture()
def cristin_ms_config1_use_orgreg_long_name_true(cristin_ms_config1_yaml):
    config = CristinMsConfigLoader(**cristin_ms_config1_yaml).cristin_ms
    return config.copy(update={"use_orgreg_long_name": True})


@pytest.fixture()
def config_loader():
    return CristinMsConfigLoader.from_file(fixtures_path() / "config1.yaml")


def fixtures_path():
    return pathlib.Path(__file__).parent / "fixtures"


def load_json_file(name):
    with (fixtures_path() / name).open() as f:
        return json.load(f)


def load_yaml_file(name):
    with (fixtures_path() / name).open() as f:
        return yaml.safe_load(f)


@pytest.fixture()
def organisasjoner_data():
    return load_json_file("organisasjoner.json").get("organisasjon")


@pytest.fixture()
def organisasjoner_objects(organisasjoner_data):
    return [Orgenhet(**org) for org in organisasjoner_data]


@pytest.fixture()
def orgreg_orgunit_objects(orgreg_list):
    return [OrgUnit(**org) for org in orgreg_list]


@pytest.fixture()
def iga_veralf2():
    return load_json_file("iga_veralf2.json")


@pytest.fixture()
def iga_olgaboe():
    return load_json_file("iga_olgaboe.json")


@pytest.fixture()
def stilling_30045705():
    return load_json_file("stilling_30045705.json")


@pytest.fixture()
def stilling_30045705_obj(stilling_30045705):
    return Stilling(**stilling_30045705)


@pytest.fixture()
def person_00101219():
    return load_json_file("person_00101219.json")


@pytest.fixture()
def ansatt_00101219(person_00101219) -> Ansatt:
    return Ansatt(**person_00101219)


@pytest.fixture()
def orgreg_list():
    return load_json_file("orgreg_list.json")


@pytest.fixture()
def greg_list():
    return load_json_file("guests.json")


@pytest.fixture()
def greg_list_objects(greg_list):
    return [greg_client.models.Person.from_dict(x) for x in greg_list["results"]]


@pytest.fixture()
def iga_all_employees(iga_veralf2, iga_olgaboe):
    return [IgaResult(**iga_veralf2), IgaResult(**iga_olgaboe)]
