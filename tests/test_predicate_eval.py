import itertools
from typing import Any

import pytest

from cristin_ms.predicate.eval import BoolAlways, BoolAnd, BoolNot, BoolOr, Predicate


@pytest.mark.parametrize(
    ("value", "arg"),
    [
        (True, True),
        (True, False),
        (True, None),
        (True, 12),
        (False, True),
        (False, False),
        (False, None),
        (False, 12),
    ],
)
def test_bool_always(*, value: bool, arg: Any):
    p: Predicate[Any] = BoolAlways(value=value)

    res = p(arg)

    assert res == value


@pytest.mark.parametrize(
    ("value", "arg"),
    [
        (True, True),
        (True, False),
        (True, None),
        (True, 12),
        (False, True),
        (False, False),
        (False, None),
        (False, 12),
    ],
)
def test_bool_not(*, value: bool, arg: Any):
    p: Predicate[Any] = BoolNot(predicate=BoolAlways(value=value))

    res = p(arg)

    assert res == (not value)


@pytest.mark.parametrize(
    ("a", "b"),
    itertools.product([True, False], [True, False]),
)
def test_bool_or(*, a: bool, b: bool):
    pa: Predicate[Any] = BoolAlways(value=a)
    pb: Predicate[Any] = BoolAlways(value=b)
    p: Predicate[Any] = BoolOr(predicates=(pa, pb))

    res = p(None)

    assert res == (a or b)


@pytest.mark.parametrize(
    ("a", "b"),
    itertools.product([True, False], [True, False]),
)
def test_bool_and(*, a: bool, b: bool):
    pa: Predicate[Any] = BoolAlways(value=a)
    pb: Predicate[Any] = BoolAlways(value=b)
    p = BoolAnd(predicates=(pa, pb))

    res = p(None)

    assert res == (a and b)
