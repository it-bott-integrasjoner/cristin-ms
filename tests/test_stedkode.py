import pydantic
import pytest

from cristin_ms.stedkode import InvalidStedkodeError, Stedkode


def test_stedkode_ok():
    x = Stedkode("123456")

    assert x.avdeling == 12
    assert x.underavdeling == 34
    assert x.gruppe == 56


def test_stedkode_ok_zero():
    x = Stedkode("000000")

    assert x.avdeling == 0
    assert x.underavdeling == 0
    assert x.gruppe == 0


@pytest.mark.parametrize("arg", ["1", "1234567", "abcdef"])
def test_stedkode_raises_value_error(arg):
    with pytest.raises(ValueError):  # noqa: PT011
        Stedkode(arg)


@pytest.mark.parametrize("arg", [None, 1, 1.0])
def test_stedkode_raises_type_error(arg):
    with pytest.raises(TypeError):
        Stedkode(arg)


@pytest.mark.parametrize(
    ("avdeling", "underavdeling", "gruppe"),
    [
        (0, 0, 0),
        (1, 2, 3),
        (10, 11, 12),
    ],
)
def test_stedkode_create_ok(avdeling, underavdeling, gruppe):
    stedkode = Stedkode.create(avdnr=avdeling, undavdnr=underavdeling, gruppenr=gruppe)

    assert stedkode.avdeling == avdeling
    assert stedkode.underavdeling == underavdeling
    assert stedkode.gruppe == gruppe


@pytest.mark.parametrize(
    ("avdeling", "underavdeling", "gruppe"),
    [
        (100, 0, 0),
        (1, 200, 3),
        (10, 11, 120),
    ],
)
def test_stedkode_create_raises_value_error(avdeling, underavdeling, gruppe):
    with pytest.raises(InvalidStedkodeError):
        Stedkode.create(avdnr=avdeling, undavdnr=underavdeling, gruppenr=gruppe)


def test_parse_with_pydantic_ok():
    value = "012345"
    stedkode = pydantic.parse_obj_as(Stedkode, value)

    assert isinstance(stedkode, Stedkode)
    assert stedkode == value


@pytest.mark.parametrize(
    "value",
    ["", 1, 0, "abcdef"],
)
def test_parse_with_pydantic_fail(value):
    with pytest.raises(pydantic.ValidationError):
        pydantic.parse_obj_as(Stedkode, value)
