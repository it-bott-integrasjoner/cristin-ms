import pytest

import cristin_ms.predicate.person_query_parser as pqp
from cristin_ms.predicate.functions import HasCerebrumConsent


@pytest.fixture(
    params=[
        "value",
        "with 'single quotes'",
        'with "double quotes"',
        "with 'single and double quotes\"",
        r"with \backslash\\",
        "with \n newline",
        "with \t tab",
    ],
)
def quoted_string(request):
    # Use repr() to create a valid quoted string
    return repr(request.param)


def test_parse_has_cerebrum_consent(quoted_string):
    query = f"HasCerebrumConsent[name={quoted_string}, type={quoted_string}]"

    expr = pqp.parse_expression_query(query)

    assert isinstance(expr, HasCerebrumConsent)
    assert str(expr) == query


def test_quoted_string(quoted_string):
    res = pqp.quoted_string.parse_string(quoted_string, parse_all=True)

    assert isinstance(res[0], str)
    assert repr(res[0]) == quoted_string
