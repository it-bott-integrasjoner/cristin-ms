import datetime

import pydantic
import pytest

from cristin_ms.frida.model import (
    AnsettelseItem,
    Brukernavn,
    EnhetItem,
    Epost,
    Etternavn,
    Fornavn,
    Stillingskode,
)


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        ("1234567890abcdef", "1234567890abcdef"),
        ("1234567890abcdef@example.com", "1234567890abcdef"),
        ("1234567890abcdef-", None),
        ("1234567890abcdef-@example.com", None),
    ],
)
def test_brukernavn(value, expected):
    x = pydantic.parse_obj_as(Brukernavn, value)

    assert x == expected


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        ("1234567890abcdef@example.com", "1234567890abcdef@example.com"),
        (
            "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef@example.com",
            None,
        ),
    ],
)
def test_epost(value, expected):
    x = pydantic.parse_obj_as(Epost, value)

    assert x == expected


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        (1234, 1234),
        (123, 123),
        (1234567890, 7890),
    ],
)
def test_stillingskode(value, expected):
    x = pydantic.parse_obj_as(Stillingskode, value)

    assert x == expected


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        ("Petterson Johansen", "Petterson Johansen"),
        ("Petterson Johansen Evensen Knutsdotter", "Johansen Evensen Knutsdotter"),
    ],
)
def test_etternavn_ok(value, expected):
    x = pydantic.parse_obj_as(Etternavn, value)

    assert x == expected


def test_etternavn_too_long():
    with pytest.raises(ValueError, match="Unable to shorten"):
        pydantic.parse_obj_as(Etternavn, "PettersonJohansenEvensenKnutsdotter")


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        ("Ole Aleksander Filibombombom", "Ole Aleksander Filibombombom"),
        ("Julius Andreas Gimli Arn MacGyver Chewbacca Highlander", "Julius Andreas Gimli Arn"),
    ],
)
def test_fornavn_ok(value, expected):
    x = pydantic.parse_obj_as(Fornavn, value)

    assert x == expected


def test_fornavn_too_long():
    with pytest.raises(ValueError, match="Unable to shorten"):
        pydantic.parse_obj_as(Fornavn, "JuliusAndreasGimliArnMacGyverChewbaccaHighlander")


def test_enhet_item_stedkoder():
    enhet = EnhetItem(
        institusjonsnr=1,
        avdnr=2,
        undavdnr=3,
        gruppenr=4,
        avdnr_under=5,
        undavdnr_under=6,
        gruppenr_under=7,
        navn_bokmal="Bokmål",
    )

    assert enhet.stedkode == "020304"
    assert enhet.parent_stedkode == "050607"


def test_ansettelse_item_stedkode():
    enhet = AnsettelseItem(
        institusjonsnr=1,
        avdnr=2,
        undavdnr=3,
        gruppenr=4,
        stillingskode=1,
        dato_fra=datetime.date.today(),
    )

    assert enhet.stedkode == "020304"
