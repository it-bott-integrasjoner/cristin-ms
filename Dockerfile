# syntax=docker/dockerfile:experimental
# Use same python version as in .gitlab-ci.yml and pyproject.toml
FROM harbor.uio.no/mirrors/docker.io/library/python:3.12-slim AS builder
ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_VIRTUALENVS_CREATE=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache \
    PIP_NO_CACHE_DIR=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    DEBIAN_FRONTEND=noninteractive

LABEL no.uio.contact=bott-int-drift@usit.uio.no

RUN apt-get update \
    && apt-get install -y --no-install-recommends git \
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install poetry==1.7.1

WORKDIR /cristin-ms/
COPY pyproject.toml poetry.lock .
RUN poetry install --only main --no-interaction --no-ansi --no-root

COPY . .
RUN poetry install --only main --no-interaction --no-ansi

FROM harbor.uio.no/mirrors/docker.io/library/python:3.12-slim AS runtime
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        openssh-client libnss-wrapper gettext-base \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /cristin-ms
ENV HOME=/cristin-ms

ENV VIRTUAL_ENV=/cristin-ms/.venv \
    PATH="/cristin-ms/.venv/bin:$PATH"

COPY --from=builder /cristin-ms/ /cristin-ms/

# Give permissions to group.
# This works since the random user chosen by OpenShift when it runs this image
# is in the root group
RUN install -v -d -m7700 /var/log/cristin-ms \
    && chgrp -R 0 . \
    && chmod -R g+rwX . \
    && chmod +x entrypoint.sh

USER nobody

ENTRYPOINT ["/cristin-ms/entrypoint.sh"]
