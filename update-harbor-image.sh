#!/bin/sh

set -ex

slugify() {
  # Turn all non-ascii non-alphanumeric characters to -, then lowercase.
  # e.g. 'feature/FOO-123_blæh_blah' -> 'feature-foo-123-bl-h-blah'
  printf %s "$1" |
    LC_ALL=C sed -E 's/[^[:alnum:]]+/-/g' |
    tr '[:upper:]' '[:lower:]'
}

shquote() {
    # Make values shell safe
    # Git tags may contain unsafe values
    LC_ALL=C awk -v q="'" '
    BEGIN{
      for (i=1; i<ARGC; i++) {
        gsub(q, q "\\" q q, ARGV[i])
        printf "%s ", q ARGV[i] q
      }
      print ""
    }' "$@"
}

# Make sure podman builds images compatible with OpenShift
export BUILDAH_FORMAT=docker

BUILDER=
REPO=${DOCKER_REGISTRY:-harbor.uio.no}
NO_CACHE=--no-cache
DO_PUSH=n

while [ $# -gt 0 ]
do
    case $1 in
        --builder)
            if [ -z "$2" ]; then
                printf '%s requires an argument\n' "$1"
                exit 1
            fi
            BUILDER=$2
            shift
            ;;
        --repo)
            if [ -z "$2" ]; then
                printf '%s requires an argument\n' "$1"
                exit 1
            fi
            REPO=$2
            shift
            ;;
        --use-cache)
          NO_CACHE=
          ;;
        --push)
          DO_PUSH=y
          ;;
        --*)
            printf 'Unknown option %s\n' "$1"
            exit 1
            ;;
    esac
    shift
done

if [ -n "$BUILDER" ]; then
    if ! command -v "$BUILDER" >/dev/null 2>&1; then
        printf 'Builder not found %s\n' "$BUILDER"
        exit 1
    fi
elif command -v podman > /dev/null 2>&1; then
    BUILDER=$(command -v podman)
elif command -v docker > /dev/null 2>&1; then
    BUILDER=$(command -v docker)
else
    echo "Missing podman or docker CLI tools"
    exit 1
fi

printf 'Will build using %s\n' "$BUILDER"

docker() {
  command "$BUILDER" "$@"
}

# Handle running the script locally, and on gitlab (otherwise, gitlab would get head as the branch name):
if [ -n "$CI_COMMIT_REF_NAME" ]; then
  GIT_BRANCH=$CI_COMMIT_REF_NAME
  GIT_SHA=$CI_COMMIT_SHORT_SHA
else
  GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD) &&
  GIT_SHA=$(git rev-parse --short HEAD) || exit
fi

PROJECT=bott-int
APP_NAME=cristin-ms
CONTAINER=$REPO/$PROJECT/$APP_NAME
IMAGE_TAG=$CONTAINER:$(slugify "$GIT_BRANCH")-$GIT_SHA

echo "Generating .dockerignore"
git ls-files | awk '
  BEGIN {
    print "*"
  }
  {
    print "!"$0
  }
  END {
    print ".*"
    print "tests/"
    print "tests/**"
    print "!entrypoint.sh"
  }
' > .dockerignore

echo "Generating entrypoint.sh"

vers_tag=$(git tag --sort=-taggerdate --points-at=HEAD | sed '$!d')

# shellcheck disable=SC2016,SC2046
printf '#!/bin/sh
set -aeu

GIT_COMMIT_SHA=%s
GIT_TAG=%s
SENTRY_RELEASE=${GIT_TAG:-$GIT_COMMIT_SHA}
if [ -n "${NSS_WRAPPER_ENABLED:-}" ]; then
  cat >/tmp/passwd <<EOF
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
app:x:$(id -u):$(id -g):Container app:${HOME}:/bin/bash
EOF
  export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libnss_wrapper.so
  export NSS_WRAPPER_PASSWD=/tmp/passwd
  export NSS_WRAPPER_GROUP=/etc/group
fi

exec "$@"
' "$(git rev-parse HEAD)" $(shquote "$vers_tag") >entrypoint.sh
# shquote properly quotes values, don't surround with quotes

echo "Building $IMAGE_TAG"

# shellcheck disable=SC2086
docker build  \
  $NO_CACHE \
  -t "$IMAGE_TAG" \
  .

if [ "$DO_PUSH" = "y" ]
then
  printf "Pushing %s\n" "$IMAGE_TAG"
  docker push "$IMAGE_TAG"
else
  printf 'To push image, run\n  %s\n' "$BUILDER push \"$IMAGE_TAG\""
fi

if [ "$GIT_BRANCH" = "master" ]
then
  echo "On master branch, setting $IMAGE_TAG as $CONTAINER:latest"
  docker tag "$IMAGE_TAG" "$CONTAINER:latest"
  if [ "$DO_PUSH" = "y" ]
  then
    printf "Pushing %s\n" "$CONTAINER:latest"
    docker push "$CONTAINER:latest"
  else
    printf 'To push image, run\n  %s\n' "$BUILDER push \"$CONTAINER:latest\""
  fi
fi

# Tag with git tag if present
vers_tag=${CI_COMMIT_TAG:-$(git tag --sort=-taggerdate --points-at=HEAD | sed 1q)}

if [ -n "$vers_tag" ]; then
  # push tagged version
  printf 'Setting %s as %s:%s\n' "$IMAGE_TAG" "$CONTAINER" "$vers_tag"
  docker tag "$IMAGE_TAG" "$CONTAINER:$vers_tag"
  if [ "$DO_PUSH" = "y" ]; then
    docker push "$CONTAINER:$vers_tag"
  fi
  printf "Release version: <%s>" "$vers_tag"
fi
